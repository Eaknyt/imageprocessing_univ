#ifndef H_EPLOT_PLOT_H
#define H_EPLOT_PLOT_H

#include <string>
#include <vector>

namespace eplot {

void plot(const std::vector<double> &points, 
          const std::vector<double> &values,
          const std::string &color = "");

void plotAgain(const std::vector<double> &points, 
               const std::vector<double> &values,
               const std::string &color = "");

void plotMulti(const std::vector<double> &points,
               const std::vector<double> &values,
               const std::string &color = "");

void plotLegend(const std::string &legend, int i = 0);
void plotTitle(const std::string &title);
void plotLabels(const std::string &xLabel, const std::string &yLabel);
void plotArea(double xFrom, double xTo, double yFrom, double yTo);
void plotHighlight(double from, double to, const std::string &color);
void plotResetHighlights();
void plotDirectory(const std::string &path);
void plotWrite(const std::string &filename);

} // namespace eplot

#endif // H_EPLOT_PLOT_H
