#ifndef H_EPWDIP_CONVERT_H
#define H_EPWDIP_CONVERT_H

#include "image.h"

namespace epwdip {

Image grayscale(const Image &img);

} // namespace epwdip

#endif // H_EPWDIP_CONVERT_H
