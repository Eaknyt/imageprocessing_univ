#ifndef H_EPWDIP_MORPHO_H
#define H_EPWDIP_MORPHO_H

#include <assert.h>
#include <initializer_list>
#include <string>

#include "image.h"


namespace epwdip {

Image erode(const Image &img);
Image dilate(const Image &img);

inline Image closeImage(const Image &img)
{
    return erode(dilate(img));
}

inline Image openImage(const Image &img)
{
    return dilate(erode(img));
}

Image difference(const Image &img1, const Image &img2);

} // namespace epwdip

#endif // H_EPWDIP_MORPHO_H
