#ifndef H_EPWDIP_IMAGEQUADTREE_H
#define H_EPWDIP_IMAGEQUADTREE_H

#include <functional>

#include "image.h"

namespace epwdip {

class Quadtree
{
public:
    using AdaptativeFunction = std::function<bool (Quadtree *)>;

    Quadtree(const Image &img, int x, int y, Quadtree *parent);
    ~Quadtree();

    Quadtree *parent() const;
    std::vector<Quadtree *> children() const;
    int depth() const;

    int x() const;
    int y() const;

    Image image() const;

    void split(int depth);
    void split(const AdaptativeFunction &func);

private:
    std::array<Image, 4> divideImage() const;

private:
    int m_x;
    int m_y;

    Image m_image;

    Quadtree *m_parent;
    std::vector<Quadtree *> m_children;
};

} // namespace epwdip

#endif // H_EPWDIP_IMAGEQUADTREE_H
