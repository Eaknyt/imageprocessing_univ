#ifndef H_EPWDIP_THRESHOLDS_H
#define H_EPWDIP_THRESHOLDS_H

#include "image.h"


namespace epwdip {

Image thresholded(const Image &img, int level);
Image thresholded(const Image &img, int level1, int level2);
Image thresholded(const Image &img, int level1, int level2, int level3);

} // namespace epwdip

#endif // H_EPWDIP_THRESHOLDS_H
