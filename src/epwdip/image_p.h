#ifndef H_EPWDIP_IMAGE_P_H
#define H_EPWDIP_IMAGE_P_H

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <vector>

namespace epwdip {

struct ImagePrivate
{
    ImagePrivate();
    ImagePrivate(int w, int h, Image::Format profile);

    void freeImage();
    void reset();

    int m_width;
    int m_height;

    Image::Format m_format;

    int m_count;

    bool m_isValid;

    std::vector<unsigned char> m_data;
    std::vector<double> m_dataD;
};


// ----------------------------------------------------------------------------
// image_ppm.c from the ICAR_Library
// ----------------------------------------------------------------------------

void ignorer_commentaires(FILE * f)
{
    unsigned char c;

    while((c=fgetc(f)) == '#')
        while((c=fgetc(f)) != '\n');

    fseek(f, -sizeof(unsigned char), SEEK_CUR);
}

void ecrire_image_ppm(const char *nom_image, const unsigned char *pt_image, int nb_lignes, int nb_colonnes)
{
    FILE *f_image;
    int taille_image = 3*nb_colonnes * nb_lignes;

    if( (f_image = fopen(nom_image, "wb")) == NULL)
    {
        printf("\nPas d'acces en ecriture sur l'image %s \n", nom_image);
        exit(EXIT_FAILURE);
    }
    else
    {
        fprintf(f_image,"P6\r") ;                               /*ecriture entete*/
        fprintf(f_image,"%d %d\r255\r", nb_colonnes, nb_lignes) ;

        if( (fwrite((unsigned char*)pt_image, sizeof(unsigned char), taille_image, f_image))
                != (size_t)(taille_image))
        {
            printf("\nErreur d'ecriture de l'image %s \n", nom_image);
            exit(EXIT_FAILURE);
        }
        fclose(f_image);
    }
}

void lire_nb_lignes_colonnes_image_ppm(const char *nom_image, int *nb_lignes, int *nb_colonnes)
{
    FILE *f_image;
    int max_grey_val;

    /* cf : l'entete d'une image .ppm : P6                   */
    /*				       nb_colonnes nb_lignes */
    /*    			       max_grey_val          */


    if( (f_image = fopen(nom_image, "rb")) == NULL)
    {
        printf("\nPas d'acces en lecture sur l'image %s \n", nom_image);
        exit(EXIT_FAILURE);
    }
    else
    {
        fscanf(f_image, "P6 ");
        ignorer_commentaires(f_image);
        fscanf(f_image, "%d %d %d%*c", nb_colonnes, nb_lignes, &max_grey_val);
        fclose(f_image);
    }
}

void lire_image_ppm(const char *nom_image, const unsigned char *pt_image, int taille_image)
{
    FILE *f_image;
    int  nb_colonnes, nb_lignes, max_grey_val;
    taille_image=3*taille_image;

    if( (f_image = fopen(nom_image, "rb")) == NULL)
    {
        printf("\nPas d'acces en lecture sur l'image %s \n", nom_image);
        exit(EXIT_FAILURE);
    }
    else
    {
        fscanf(f_image, "P6 ");
        ignorer_commentaires(f_image);
        fscanf(f_image, "%d %d %d%*c",
               &nb_colonnes, &nb_lignes, &max_grey_val); /*lecture entete*/

        if( (fread((unsigned char*)pt_image, sizeof(unsigned char), taille_image, f_image))
                !=  (size_t)(taille_image))
        {
            printf("\nErreur de lecture de l'image %s \n", nom_image);
            exit(EXIT_FAILURE);
        }
        fclose(f_image);
    }
}

void ecrire_image_pgm(const char *nom_image, const unsigned char *pt_image, int nb_lignes, int nb_colonnes)
{
    FILE *f_image;
    int taille_image = nb_colonnes * nb_lignes;

    if( (f_image = fopen(nom_image, "wb")) == NULL)
    {
        printf("\nPas d'acces en ecriture sur l'image %s \n", nom_image);
        exit(EXIT_FAILURE);
    }
    else
    {
        fprintf(f_image,"P5\r") ;                               /*ecriture entete*/
        fprintf(f_image,"%d %d\r255\r", nb_colonnes, nb_lignes) ;

        if( (fwrite((unsigned char*)pt_image, sizeof(unsigned char), taille_image, f_image))
                != (size_t) taille_image)
        {
            printf("\nErreur de lecture de l'image %s \n", nom_image);
            exit(EXIT_FAILURE);
        }
        fclose(f_image);
    }
}

void lire_nb_lignes_colonnes_image_pgm(const char *nom_image, int *nb_lignes, int *nb_colonnes)
{
    FILE *f_image;
    int max_grey_val;

    /* cf : l'entete d'une image .pgm : P5                    */
    /*				       nb_colonnes nb_lignes */
    /*    			       max_grey_val          */


    if( (f_image = fopen(nom_image, "rb")) == NULL)
    {
        printf("\nPas d'acces en lecture sur l'image %s \n", nom_image);
        exit(EXIT_FAILURE);
    }
    else
    {
        fscanf(f_image, "P5 ");
        ignorer_commentaires(f_image);
        fscanf(f_image, "%d %d %d%*c", nb_colonnes, nb_lignes, &max_grey_val);
        fclose(f_image);
    }
}

void lire_image_pgm(const char *nom_image, const unsigned char *pt_image, int taille_image)
{
    FILE *f_image;
    int  nb_colonnes, nb_lignes, max_grey_val;

    if( (f_image = fopen(nom_image, "rb")) == NULL)
    {
        printf("\nPas d'acces en lecture sur l'image %s \n", nom_image);
        exit(EXIT_FAILURE);
    }
    else
    {
        fscanf(f_image, "P5 ");
        ignorer_commentaires(f_image);
        fscanf(f_image, "%d %d %d%*c",
               &nb_colonnes, &nb_lignes, &max_grey_val); /*lecture entete*/

        if( (fread((unsigned char*)pt_image, sizeof(unsigned char), taille_image, f_image))
                !=  (size_t) taille_image)
        {
            printf("\nErreur de lecture de l'image %s \n", nom_image);
            exit(EXIT_FAILURE);
        }
        fclose(f_image);
    }
}

} // namespace epwdip

#endif // H_EPWDIP_IMAGE_P_H
