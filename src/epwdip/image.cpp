#include "image.h"
#include "image_p.h"

#include <algorithm>
#include <assert.h>
#include <iostream>
#include <vector>

namespace {

const char ERROR_MSG_SAVE[] = "ImageBase::write() > the image is not valid";
const char ERROR_MSG_CTOR_DATA[] = "Image::Image(data, w, h, format) > Invalid dimensions or format";

} // anon namespace


namespace epwdip {


////////////////////////// ImagePrivate //////////////////////////

ImagePrivate::ImagePrivate() :
    m_width(0),
    m_height(0),
    m_format(Image::Format::Rgb888Format),
    m_isValid(false),
    m_count(0),
    m_data(),
    m_dataD()
{}

ImagePrivate::ImagePrivate(int w, int h, Image::Format format) :
    m_width(w),
    m_height(h),
    m_format(format),
    m_count(w * h * ((format != Image::Format::Rgb888Format) ? 1 : 3)),
    m_isValid(m_count > 0),
    m_data(m_count),
    m_dataD(m_count)
{}

void ImagePrivate::freeImage()
{
    m_data.clear();
    m_dataD.clear();
}

void ImagePrivate::reset()
{
    freeImage();

    m_width = m_height = 0;
    m_format = Image::Format::Grayscale8Format;
    m_count = 0;
    m_isValid = false;
}


////////////////////////// Image //////////////////////////

Image::Image() :
    d(new ImagePrivate)
{}

Image::Image(int w, int h, Format format) :
    d(new ImagePrivate(w, h, format))
{}

Image::Image(const unsigned char *data, int w, int h, Image::Format format) :
    d(new ImagePrivate(w, h, format))
{
    for (int i = 0; i < d->m_count; ++i) {
        d->m_data[i] = data[i];
        d->m_dataD[i] = data[i];
    }
}

Image::Image(const std::vector<unsigned char> &data, int w, int h,
             Image::Format format) :
    d(new ImagePrivate(w, h, format))
{
    if (d->m_data.size() != data.size()) {
        std::cerr << ERROR_MSG_CTOR_DATA << std::endl;
    }
    else {
        std::copy(data.begin(), data.end(), d->m_data.begin());
        std::copy(data.begin(), data.end(), d->m_dataD.begin());
    }
}

Image::Image(const std::string &filename) :
    d(new ImagePrivate)
{
    read(filename);
}

Image::Image(const Image &other) :
    d(new ImagePrivate)
{
    copy(other);
}

Image::~Image()
{
    delete d;
}

int Image::width() const
{
    return d->m_width;
}

int Image::height() const
{
    return d->m_height;
}

bool Image::isNull() const
{
    return d->m_count == 0;
}

bool Image::isValid() const
{
    return d->m_isValid;
}

std::size_t Image::size() const
{
    return d->m_count;
}

int Image::pixelCount() const
{
    return (d->m_format == Format::Rgb888Format) ? d->m_count / 3
                                                 : d->m_count;
}

Image::Format Image::format() const
{
    return d->m_format;
}

Image Image::convert(Image::Format f) const
{
    Image ret = (*this);

    if (d->m_format == Format::Grayscale8Format) {
        if (f == Format::BinaryFormat) {
            ret.d->m_format = Format::BinaryFormat;
        }
    }
    else if (d->m_format == Format::BinaryFormat) {
        if (f == Format::Grayscale8Format) {
            ret.d->m_format = Format::Grayscale8Format;
        }
    }
    else if (d->m_format == Format::Rgb888Format) {
        if (f == Format::Grayscale8Format) {
            ret.d->m_format = Format::Grayscale8Format;
        }
    }
    return ret;
}

bool Image::isGrayscale() const
{
    return d->m_format == Format::Grayscale8Format ||
            d->m_format == Format::BinaryFormat;
}

bool Image::isColored() const
{
    return d->m_format == Format::Rgb888Format;
}

unsigned char *Image::data()
{
    return d->m_data.data();
}

const unsigned char *Image::data() const
{
    return d->m_data.data();
}

Image Image::subImage(int x, int y, int w, int h) const
{
    assert (x >= 0 && y >= 0);
    assert (w > 0 && h > 0);
    assert (d->m_format == Format::Grayscale8Format);

    const int lx = x + w;
    const int ly = y + h;

    assert (lx < d->m_width);
    assert (ly < d->m_height);

    Image ret(w, h, d->m_format);

    for (int ret_y = 0; ret_y < h; ++ret_y) {
        for (int ret_x = 0; ret_x < w; ++ret_x) {
            const int ix = x + ret_x;
            const int iy = y + ret_y;

            ret(ret_x, ret_y) = operator()(ix, iy);
        }
    }

    return ret;
}

Color Image::pixel(int x, int y) const
{
    const bool useGrayscaleOp = isGrayscale();

    const unsigned char r = useGrayscaleOp ? operator()(x, y) : operator()(x, y, Channel::RedChannel);
    const unsigned char g = useGrayscaleOp ? operator()(x, y) : operator()(x, y, Channel::GreenChannel);
    const unsigned char b = useGrayscaleOp ? operator()(x, y) : operator()(x, y, Channel::BlueChannel);

    Color ret = Color::fromRgb(r, g, b);

    return ret;
}

void Image::setPixel(int x, int y, const Color &color)
{
    const bool isGrayscale = (d->m_format != Image::Format::Rgb888Format);

    if (isGrayscale) {
        operator()(x, y) = color.red();
    }
    else {
        operator()(x, y, Channel::RedChannel) = color.red();
        operator()(x, y, Channel::GreenChannel) = color.green();
        operator()(x, y, Channel::BlueChannel) = color.blue();
    }
}

bool Image::read(const std::string &filename)
{
    d->reset();

    std::size_t filenameSize = filename.size();

    // Le fichier ne peut pas etre que ".pgm" ou ".ppm"
    if (filenameSize <= 4) {
        return false;
    }

    int nbPixel = 0;

    const std::string ext = filename.substr(filenameSize - 3, filenameSize);

    // L'image est en niveau de gris
    if (ext.compare("pgm") == 0) {
        d->m_format = Format::Grayscale8Format;
        lire_nb_lignes_colonnes_image_pgm(filename.c_str(), &d->m_height, &d->m_width);
        nbPixel = d->m_height * d->m_width;

        d->m_count = nbPixel;
        d->m_data.resize(d->m_count);
        lire_image_pgm(filename.c_str(), d->m_data.data(), nbPixel);
    }
    // L'image est en couleur
    else if (ext.compare("ppm") == 0) {
        d->m_format = Format::Rgb888Format;
        lire_nb_lignes_colonnes_image_ppm(filename.c_str(), &d->m_height, &d->m_width);
        nbPixel = d->m_height * d->m_width;

        d->m_count = nbPixel * 3;
        d->m_data.resize(d->m_count);
        lire_image_ppm(filename.c_str(), d->m_data.data(), nbPixel);
    }
    else {
        return false;
    }

    d->m_dataD.reserve(d->m_count);

    d->m_isValid = true;

    return true;
}

bool Image::write(const std::string &filename) const
{
    if (!d->m_isValid) {
        std::cerr << ERROR_MSG_SAVE << std::endl;
        return false;
    }

    if (d->m_format == Format::Rgb888Format) {
        ecrire_image_ppm(filename.c_str(), d->m_data.data(),
                         d->m_height, d->m_width);
    }
    else {
        ecrire_image_pgm(filename.c_str(), d->m_data.data(),
                         d->m_height, d->m_width);
    }

    return true;
}

void Image::clear(unsigned char gray)
{
    for (int i = 0; i < d->m_count; ++i) {
        d->m_data[i] = gray;
    }
}

void Image::clear(unsigned char r, unsigned char g, unsigned char b)
{
    if (d->m_format == Format::Grayscale8Format) {
        return;
    }

    for (int y = 0; y < d->m_height; ++y) {
        for (int x = 0; x < d->m_width; ++x) {
            operator()(x, y, Channel::RedChannel) = r;
            operator()(x, y, Channel::GreenChannel) = g;
            operator()(x, y, Channel::BlueChannel) = b;
        }
    }
}

void Image::copy(const Image &other)
{
    d->freeImage();

    d->m_width = other.d->m_width;
    d->m_height = other.d->m_height;
    d->m_format = other.d->m_format;
    d->m_count = other.d->m_count;
    d->m_isValid = other.d->m_isValid;

    if (d->m_count == 0) {
        return;
    }

    d->m_data.resize(d->m_count);
    d->m_dataD.resize(d->m_count);

    d->m_isValid = true;

    for (int i = 0; i < d->m_count; ++i) {
        d->m_data[i] = other.d->m_data[i];
        d->m_dataD[i] = other.d->m_dataD[i];
    }
}

const unsigned char Image::operator()(int x, int y) const
{
    assert (d->m_format != Image::Format::Rgb888Format);

    assert (0 <= x && x < d->m_width);
    assert (0 <= y && y < d->m_height);

    return d->m_data[x + y * d->m_width];
}

unsigned char &Image::operator()(int x, int y)
{
    assert (d->m_format != Image::Format::Rgb888Format);
    assert (0 <= x && x < d->m_width);
    assert (0 <= y && y < d->m_height);

    return d->m_data[x + y * d->m_width];
}

const unsigned char Image::operator()(int x, int y, Channel c) const
{
    assert (d->m_format == Image::Format::Rgb888Format);

    assert (0 <= x && x < d->m_width);
    assert (0 <= y && y < d->m_height);

    const int rgbOffset = static_cast<int>(c);

    return d->m_data[x * 3 + rgbOffset + y * (d->m_width * 3)];
}

unsigned char &Image::operator()(int x, int y, Channel c)
{
    assert (d->m_format == Image::Format::Rgb888Format);

    assert (0 <= x && x < d->m_width);
    assert (0 <= y && y < d->m_height);

    const int rgbOffset = static_cast<int>(c);

    return d->m_data[x * 3 + rgbOffset + y * (d->m_width * 3)];
}

Image &Image::operator=(const Image &other)
{
    copy(other);

    return *this;
}

} // namespace epwdip
