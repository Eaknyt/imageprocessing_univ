#ifndef H_EPWDIP_RECT_H
#define H_EPWDIP_RECT_H

namespace epwdip {

class Rect
{
public:
    Rect();
    Rect(int left, int top, int right, int bottom);

    bool isValid() const;
    bool isNegative() const;
    bool isEmpty() const;
    bool isNull() const;

    int left() const;
    void setLeft(int val);

    int top() const;
    void setTop(int top);

    int right() const;
    void setRight(int val);

    int bottom() const;
    void setBottom(int val);

    int width() const;
    int height() const;

    int x() const
    {
        return left();
    }

    int y() const
    {
        return top();
    }

private:
    int m_left;
    int m_top;

    int m_right;
    int m_bottom;
};

} // namespace epwdip

#endif // H_EPWDIP_RECT_H
