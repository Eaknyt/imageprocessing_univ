#ifndef H_EPWDIP_GLOBAL_H
#define H_EPWDIP_GLOBAL_H

namespace epwdip {

enum class Channel : int
{
    RedChannel,
    GreenChannel,
    BlueChannel
};

} // namespace epwdip

#endif // H_EPWDIP_GLOBAL_h
