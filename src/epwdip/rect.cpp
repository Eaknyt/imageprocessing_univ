#include "rect.h"

namespace epwdip {

Rect::Rect() :
    m_left(0),
    m_top(0),
    m_right(0),
    m_bottom(0)
{}

Rect::Rect(int left, int top, int right, int bottom) :
    m_left(left),
    m_top(top),
    m_right(right),
    m_bottom(bottom)
{}

bool Rect::isValid() const
{
    return width() > 0 && height() > 0;
}

bool Rect::isNegative() const
{
    return width() < 0 || height() < 0;
}

bool Rect::isEmpty() const
{
    return width() == 0 || height() == 0;
}

bool Rect::isNull() const
{
    return (!m_left && !m_top && !m_right && !m_bottom);
}

int Rect::left() const
{
    return m_left;
}

void Rect::setLeft(int val)
{
    if (m_left != val) {
        m_left = val;
    }
}

int Rect::top() const
{
    return m_top;
}

void Rect::setTop(int val)
{
    if (m_top != val) {
        m_top = val;
    }
}

int Rect::right() const
{
    return m_right;
}

void Rect::setRight(int val)
{
    if (m_right != val) {
        m_right = val;
    }
}

int Rect::bottom() const
{
    return m_bottom;
}

void Rect::setBottom(int val)
{
    if (m_bottom != val) {
        m_bottom = val;
    }
}

int Rect::width() const
{
    return m_right - m_left;
}

int Rect::height() const
{
    return m_bottom - m_top;
}

} // namespace epwdip
