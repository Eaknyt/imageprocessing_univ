#include "inpaint.h"

#include <array>
#include <assert.h>
#include <cmath>
#include <iostream>
#include <numeric>

#include "../epwdip/morpho.h"

namespace {

bool hasWhitePixel(const epwdip::Image &img)
{
    assert (img.isGrayscale());

    for (int y = 0; y < img.height(); ++y) {
        for (int x = 0; x < img.width(); ++x) {
            if (img(x, y) == 255) {
                return true;
            }
        }
    }

    return false;
}

int sobelConvolution(const epwdip::Image &img, int x, int y)
{
    const std::array<int, 9> kernelGx = {
        -1, 0, 1,
        -2, 0, 2,
        -1, 0, 1
    };
    const std::array<int, 9> kernelGy = {
        1, 2, 1,
        0, 0, 0,
        -1, -2, -1
    };

    int Gx = 0, Gy = 0;

    for (int u = -1; u <= 1; ++u) {
        for (int v = -1; v <= 1; ++v) {
            int pixX = x - v;
            int pixY = y - u;

            if (pixX >= 0 && pixX < img.width() && pixY >= 0 && pixY < img.height()) {
                Gx += img(pixX, pixY) * kernelGx[(kernelGx.size() - 1) - (v+1) - ((u+1)*3)];
                Gy += img(pixX, pixY) * kernelGy[(kernelGy.size() - 1) - (v+1) - ((u+1)*3)];
            }
        }
    }

    return sqrt(Gx * Gx  + Gy * Gy);
}

float gradientWeights(const epwdip::Image &img, int x, int y, int alphaGrad)
{
    int grad = sobelConvolution(img, x, y);

    if (abs(grad) <= alphaGrad / 2) {
        return 1.f - std::pow(float(grad) / float(alphaGrad), 2);
    }
    else if (abs(grad) > alphaGrad / 2 && abs(grad) <= alphaGrad) {
        return std::pow(float(grad) / float(alphaGrad)- 1.f, 2);
    }

    return 0;
}

} // anon namespace


namespace epwdip {

Image inpaint_kernel(const Image &img, const Image &mask, const Color &maskColor, int iterations)
{
    if (mask.isNull()) {
        return Image();
    }

    const int w = img.width();
    const int h = img.height();

    Image ret = img;

    Image mutableMask = mask.convert(Image::Format::BinaryFormat);

    const std::array<float, 8> kernel = {
        0.073235f, 0.176765f, 0.073235f,
        0.176765f,            0.176765f,
        0.073235f, 0.176765f, 0.073235f
    };

    while (hasWhitePixel(mutableMask)) {
        for (int i = 0; i < iterations; ++i) {
            for (int y = 0; y < h; ++y) {
                for (int x = 0; x < w; ++x) {
                    if (mutableMask(x, y) != 255) {
                        continue;
                    }

                    int pxColor = ret(x - 1, y - 1) * kernel[7] + ret(x, y - 1) * kernel[6] +
                            ret(x + 1, y - 1) * kernel[5] +
                            ret(x - 1, y) * kernel[4] + ret(x + 1, y) * kernel[3] +
                            ret(x - 1, y + 1) * kernel[2] + ret(x, y + 1) * kernel[1] +
                            ret(x + 1, y + 1) * kernel[0];

                    ret(x, y) = pxColor;
                }
            }
        }

        mutableMask = erode(mutableMask);
    }

    return ret;
}

Image inpaint_convolution(const Image &img, const Image &mask,
                          const Color &maskColor, int iterations)
{
    if (mask.isNull()) {
        return Image();
    }

    const int w = img.width();
    const int h = img.height();

    Image ret = img;

    //for (int i=0; i < iterations; ++i)
    //{
    Image mutableMask = mask.convert(Image::Format::BinaryFormat);

    int estimGradient = 0;

    while (hasWhitePixel(mutableMask)) {
        for (int y = 1; y < h - 1; ++y) {
            for (int x = 1; x < w - 1; ++x) {
                if (mutableMask(x, y) == 255 && (mutableMask(x - 1, y - 1) == 0 ||
                                                 mutableMask(x, y - 1) == 0 ||
                                                 mutableMask(x + 1, y - 1) == 0 ||
                                                 mutableMask(x - 1, y) == 0 ||
                                                 mutableMask(x + 1, y) == 0 ||
                                                 mutableMask(x - 1, y + 1) == 0 ||
                                                 mutableMask(x, y + 1) == 0 ||
                                                 mutableMask(x + 1, y + 1) == 0)) {
                    if (estimGradient == 0)
                        estimGradient = sobelConvolution(mutableMask, x, y);

                    // Find the know pixels
                    std::vector<std::pair<int, int>> knowPix;
                    if (mutableMask(x - 1, y - 1) == 0)
                        knowPix.push_back(std::pair<int, int>(x - 1, y - 1));
                    if (mutableMask(x, y - 1) == 0) knowPix.push_back(std::pair<int, int>(x, y - 1));
                    if (mutableMask(x + 1, y - 1) == 0)
                        knowPix.push_back(std::pair<int, int>(x + 1, y - 1));
                    if (mutableMask(x - 1, y) == 0) knowPix.push_back(std::pair<int, int>(x - 1, y));
                    if (mutableMask(x + 1, y) == 0) knowPix.push_back(std::pair<int, int>(x + 1, y));
                    if (mutableMask(x - 1, y + 1) == 0)
                        knowPix.push_back(std::pair<int, int>(x - 1, y + 1));
                    if (mutableMask(x, y + 1) == 0) knowPix.push_back(std::pair<int, int>(x, y + 1));
                    if (mutableMask(x + 1, y + 1) == 0)
                        knowPix.push_back(std::pair<int, int>(x + 1, y + 1));

                    float wk = 0;
                    float wkfk = 0;

                    for (auto &pix:knowPix) {
                        wk += (1.f / float(knowPix.size())) *
                                gradientWeights(img, pix.first, pix.second, estimGradient);
                        wkfk += (1.f / float(knowPix.size())) *
                                gradientWeights(img, pix.first, pix.second, estimGradient) *
                                ret(pix.first, pix.second);
                    }

                    ret(x, y) = (1 - wk) * ret(x, y) + wkfk;
                }
            }
        }

        for (int y = h - 2; y > 1; --y) {
            for (int x = w - 2; x > 1; --x) {
                if (mutableMask(x, y) == 255 && (mutableMask(x - 1, y - 1) == 0 ||
                                                 mutableMask(x, y - 1) == 0 ||
                                                 mutableMask(x + 1, y - 1) == 0 ||
                                                 mutableMask(x - 1, y) == 0 ||
                                                 mutableMask(x + 1, y) == 0 ||
                                                 mutableMask(x - 1, y + 1) == 0 ||
                                                 mutableMask(x, y + 1) == 0 ||
                                                 mutableMask(x + 1, y + 1) == 0)) {
                    // Find the know pixels
                    std::vector<std::pair<int, int>> knowPix;
                    if (mutableMask(x - 1, y - 1) == 0)
                        knowPix.push_back(std::pair<int, int>(x - 1, y - 1));
                    if (mutableMask(x, y - 1) == 0) knowPix.push_back(std::pair<int, int>(x, y - 1));
                    if (mutableMask(x + 1, y - 1) == 0)
                        knowPix.push_back(std::pair<int, int>(x + 1, y - 1));
                    if (mutableMask(x - 1, y) == 0) knowPix.push_back(std::pair<int, int>(x - 1, y));
                    if (mutableMask(x + 1, y) == 0) knowPix.push_back(std::pair<int, int>(x + 1, y));
                    if (mutableMask(x - 1, y + 1) == 0)
                        knowPix.push_back(std::pair<int, int>(x - 1, y + 1));
                    if (mutableMask(x, y + 1) == 0) knowPix.push_back(std::pair<int, int>(x, y + 1));
                    if (mutableMask(x + 1, y + 1) == 0)
                        knowPix.push_back(std::pair<int, int>(x + 1, y + 1));

                    float wk = 0;
                    float wkfk = 0;

                    for (auto &pix:knowPix) {
                        wk += (1.f / float(knowPix.size())) *
                                gradientWeights(img, pix.first, pix.second, estimGradient);
                        wkfk += (1.f / float(knowPix.size())) *
                                gradientWeights(img, pix.first, pix.second, estimGradient) *
                                ret(pix.first, pix.second);
                    }

                    ret(x, y) = (1 - wk) * ret(x, y) + wkfk;
                }
            }
        }

        mutableMask = erode(mutableMask);
    }

    return ret;
}

Image inpaint_anisotropic(const Image &img, const Image &mask,
                          const Color &maskColor, int iterations)
{
    const int w = img.width();
    const int h = img.height();

    return Image();
}

} // namespace epwdip
