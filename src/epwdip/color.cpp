#include "color.h"

#include <iostream>


namespace {

const epwdip::Color globalColors[] = {
	epwdip::Color::fromRgb(0, 0, 0),
	epwdip::Color::fromRgb(255, 255, 255),
	epwdip::Color::fromRgb(128, 128, 128),
	epwdip::Color::fromRgb(160, 160, 164),
	epwdip::Color::fromRgb(192, 192, 192),
	epwdip::Color::fromRgb(255, 0, 0),
	epwdip::Color::fromRgb(0, 255, 0),
	epwdip::Color::fromRgb(0, 0, 255),
	epwdip::Color::fromRgb(0, 255, 255),
	epwdip::Color::fromRgb(255, 0, 255),
	epwdip::Color::fromRgb(255, 255, 0),
	epwdip::Color::fromRgb(128, 0, 0),
	epwdip::Color::fromRgb(0, 128, 0),
	epwdip::Color::fromRgb(0, 0, 128),
	epwdip::Color::fromRgb(0, 128, 128),
	epwdip::Color::fromRgb(128, 0, 128),
	epwdip::Color::fromRgb(128, 128, 0),
	epwdip::Color::fromRgb(0, 0, 0, 0),
	epwdip::Color(0.24f, 0.73f, 0.13f),
	epwdip::Color(0.26f, 0.54f, 1.0f)
};

} // anon namespace

namespace epwdip {

Color::Color() :
	m_r(0.),
	m_g(0.),
	m_b(0.),
	m_a(1.)
{}

Color::Color(float r, float g, float b, float a) :
	m_r(r > 1. ? 0. : r),
	m_g(g > 1. ? 0. : g),
	m_b(b > 1. ? 0. : b),
	m_a(a > 1. ? 0. : a)
{}

Color::Color(GlobalColor color)
{
	Color col = globalColors[static_cast<int>(color)];

	m_r = col.m_r;
	m_g = col.m_g;
	m_b = col.m_b;
	m_a = col.m_a;
}

Color::Color(const Color &other) :
	m_r(other.m_r),
	m_g(other.m_g),
	m_b(other.m_b),
	m_a(other.m_a)
{}

Color Color::fromRgb(unsigned char r, unsigned char g, unsigned char b,
		  			 unsigned char a)
{
    const float factor = 1. / 255.;

    Color ret(factor * r,
              factor * g,
              factor * b,
              factor * a);

    return ret;
}

void Color::toRgb(unsigned char *r, unsigned char *g, unsigned char *b) const
{
    *r = m_r * 255;
    *g = m_g * 255;
    *b = m_b * 255;
}

unsigned char Color::red() const
{
    return m_r * 255;
}

unsigned char Color::green() const
{
    return m_g * 255;
}

unsigned char Color::blue() const
{
    return m_b * 255;
}

unsigned char Color::alpha() const
{
    return m_a * 255;
}

float Color::redF() const
{
	return m_r;
}

void Color::setRedF(float r)
{
	if (m_r != r) {
		m_r = r;
	}
}

float Color::greenF() const
{
	return m_g;
}

void Color::setGreenF(float g)
{
	if (m_g != g) {
		m_g = g;
	}
}

float Color::blueF() const
{
	return m_b;
}

void Color::setBlueF(float b)
{
	if (m_b != b) {
		m_b = b;
	}
}

float Color::alphaF() const
{
	return m_a;
}

void Color::setAlphaF(float a)
{
	if (m_a != a) {
		m_a = a;
	}
}

Color &Color::operator=(const Color &color)
{
	m_r = color.m_r;
	m_g = color.m_g;
	m_b = color.m_b;
	m_a = color.m_a;

	return *this;
}

Color &Color::operator=(GlobalColor color)
{
    return operator=(Color(color));
}

const Color operator+(const Color &lhs, const Color &rhs)
{
    Color ret(lhs.m_r + rhs.m_r,
              lhs.m_g + rhs.m_g,
              lhs.m_b + rhs.m_b);

    return ret;
}

const Color operator*(const Color &lhs, float factor)
{
    Color ret(lhs.m_r * factor,
              lhs.m_g * factor,
              lhs.m_b * factor);

    return ret;
}

bool operator==(const Color &lhs, const Color &rhs)
{
    if (lhs.m_r != rhs.m_r) {
        return false;
    }

    if (lhs.m_g != rhs.m_g) {
        return false;
    }

    if (lhs.m_b != rhs.m_b) {
        return false;
    }

    if (lhs.m_a != rhs.m_a) {
        return false;
    }

    return true;
}

bool operator!=(const Color &lhs, const Color &rhs)
{
    return !(lhs == rhs);
}

std::ostream &operator<<(std::ostream &os, const Color &color)
{
    os << "epwdip::Color("
       << static_cast<int>(color.red()) << ", "
       << static_cast<int>(color.green()) << ", "
       << static_cast<int>(color.blue()) << ", "
       << static_cast<int>(color.alpha())
       << ")";

    return os;
}

} // namespace epwdip
