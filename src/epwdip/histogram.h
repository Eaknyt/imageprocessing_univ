#ifndef H_EPWDIP_HISTOGRAM_H
#define H_EPWDIP_HISTOGRAM_H

#include <array>
#include <vector>

#include "channels.h"

namespace epwdip {

class Image;

std::vector<int> histogram(const Image &img);
std::vector<int> histogram(const Image &img, Channel channel);

std::array<std::vector<int>, 3> histogramRgb(const Image &img);

Image histExpand(const Image &img, float *a = nullptr, float *b = nullptr);

std::vector<double> histCfd(const std::vector<int> &histo, int pixelCount);

Image histEqualize(const Image &img, const std::vector<double> &cfd);

} // namespace epwdip

#endif // H_EPWDIP_HISTOGRAM_H
