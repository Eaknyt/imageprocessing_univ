#ifndef H_EPWDIP_INPAINT_H
#define H_EPWDIP_INPAINT_H

#include "color.h"
#include "image.h"
#include "rect.h"

namespace epwdip {

enum class InpaintMethod
{
    GaussianKernel,
    AnisotropicDiffusion
};

Image inpaint_kernel(const Image &img, const Image &mask, const Color &maskColor, int iterations);
Image inpaint_convolution(const Image &img, const Image &mask, const Color &maskColor, int iterations);
Image inpaint_anisotropic(const Image &img, const Image &mask, const Color &maskColor, int iterations);

} // namespace epwdip

#endif // H_EPWDIP_INPAINT_H
