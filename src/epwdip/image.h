#ifndef H_EPWDIP_IMAGE_H
#define H_EPWDIP_IMAGE_H

#include <string>
#include <vector>

#include "epwdip_global.h"
#include "color.h"

namespace epwdip {

struct ImagePrivate;

class Image
{
public:
    enum class Format : short
    {
        BinaryFormat,
        Grayscale8Format,
        Rgb888Format,
    };

public:
    Image();
    Image(int w, int h, Format format);
    Image(const unsigned char *data, int w, int h, Image::Format format);
    Image(const std::vector<unsigned char> &data, int w, int h, Format format);
    Image(const std::string &filename);
    Image(const Image &other);
    ~Image();

    int width() const;
    int height() const;

    bool isNull() const;
    bool isValid() const;

    std::size_t size() const;
    int pixelCount() const;

    Format format() const;
    Image convert(Format f) const;

    bool isGrayscale() const;
    bool isColored() const;

    unsigned char *data();
    const unsigned char *data() const;

    Image subImage(int x, int y, int w, int h) const;

    Color pixel(int x, int y) const;
    void setPixel(int x, int y, const Color &color);

    void clear(unsigned char gray);
    void clear(unsigned char r, unsigned char g, unsigned char b);

    void copy(const Image &other);

    bool read(const std::string &filename);
    bool write(const std::string &filename) const;

    const unsigned char operator()(int x, int y) const;
    unsigned char &operator()(int x, int y);

    const unsigned char operator()(int x, int y, Channel c) const;
    unsigned char &operator()(int x, int y, Channel c);

    Image &operator=(const Image &other);

private:
    ImagePrivate *d;
};

} // namespace epwdip

#endif // H_EPWDIP_IMAGE_H
