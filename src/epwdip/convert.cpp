#include "convert.h"
#include <iostream>

namespace epwdip {

Image grayscale(const Image &img)
{
    if (img.format() != Image::Format::Rgb888Format) {
        return img;
    }

    std::vector<unsigned char> grayscales;
    grayscales.reserve(img.width() * img.height());

    for (int i = 0; i < img.width() * img.height() * 3; i += 3) {
        const unsigned char r = img.data()[i];
        const unsigned char g = img.data()[i+1];
        const unsigned char b = img.data()[i+2];

        const unsigned char gs = r * 0.3 + g * 0.6 + b * 0.1;

        grayscales.push_back(gs);
    }

    Image ret(grayscales, img.width(), img.height(),
              Image::Format::Grayscale8Format);

    return ret;
}

} // namespace epwdip
