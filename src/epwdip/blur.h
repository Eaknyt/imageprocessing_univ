#ifndef H_EWPDIP_BLUR_H
#define H_EWPDIP_BLUR_H

#include "image.h"

namespace epwdip {

Image blur(const Image &img);
Image blur(const Image &img, const Image &binaryMask);

} // namespace epwdip

#endif // H_EWPDIP_BLUR_H
