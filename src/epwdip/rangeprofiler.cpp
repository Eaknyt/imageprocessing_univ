#include "rangeprofiler.h"

#include <iostream>

#include "image.h"


namespace epwdip {

std::vector<int> profileRow(const Image &img, int row)
{
    Image::Format colorFormat = img.format();
    int h = img.height();
    int isGrayscale = (colorFormat != Image::Format::Rgb888Format);

    if (!isGrayscale) {
        std::cerr << "RangeProfiler::profileRow() > img must be grayscale"
                  << std::endl;
        return std::vector<int>();
    }

    if (row >= h) {
        std::cerr << "RangeProfiler::profileRow() > invalid row" << std::endl;
        return std::vector<int>();
    }

    int w = img.width();
    std::vector<int> ret(w);

    for (int x = 0; x < w; ++x) {
        ret[x] = static_cast<int>(img(x, row));
    }

    return ret;
}

std::vector<int> profileCol(const Image &img, int col)
{
    Image::Format colorFormat = img.format();
    int w = img.width();
    int isGrayscale = (colorFormat != Image::Format::Rgb888Format);

    if (!isGrayscale) {
        std::cerr << "RangeProfiler::profileCol() > img must be grayscale"
                  << std::endl;
        return std::vector<int>();
    }

    if (col >= w) {
        std::cerr << "RangeProfiler::profileCol() > invalid column" << std::endl;
        return std::vector<int>();
    }

    int h = img.height();
    std::vector<int> ret(w);

    for (int y = 0; y < h; ++y) {
        ret[y] = static_cast<int>(img(col, y));
    }

    return ret;
}

} // namespace epwdip
