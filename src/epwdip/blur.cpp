#include "blur.h"

#include <assert.h>

namespace epwdip {

Image blur(const Image &img)
{
    assert (img.format() == Image::Format::Rgb888Format);

    Image ret = img;

    for (int y = 1; y < img.height() - 1; ++y) {
        for (int x = 1; x < img.width() - 1; ++x) {
            for (int channelOffset = 0; channelOffset < 3; ++channelOffset) {
                Channel c = Channel(channelOffset);

                int v = img(x - 1, y - 1, c) + img(x, y - 1, c) + img(x + 1, y - 1, c) +
                        img(x - 1, y, c) + img(x, y, c) + img(x + 1, y, c) +
                        img(x - 1, y + 1, c) + img(x, y + 1, c) + img(x + 1, y + 1, c);

                v /= 9;

                ret(x, y, c) = v;
            }
        }
    }

    return ret;
}

Image blur(const Image &img, const Image &binaryMask)
{
    const int w = binaryMask.width();
    const int h = binaryMask.height();

    assert (w == img.width() && h == img.height());

    Image ret = img;
    const Image blurredImg = blur(img);

    for (int y = 0; y < h; ++y) {
        for (int x = 0; x < w; ++x) {
            bool useBackground = (binaryMask(x, y) == 0);

            if (useBackground) {
                ret(x, y, Channel::RedChannel) = blurredImg(x, y, Channel::RedChannel);
                ret(x, y, Channel::GreenChannel) = blurredImg(x, y, Channel::GreenChannel);
                ret(x, y, Channel::BlueChannel) = blurredImg(x, y, Channel::BlueChannel);
            }
        }
    }

    return ret;
}

} // namespace epwdip
