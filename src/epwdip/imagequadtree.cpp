#include "imagequadtree.h"

#include <array>

namespace epwdip {

Quadtree::Quadtree(const Image &img, int x, int y, Quadtree *parent) :
    m_x(x),
    m_y(y),
    m_image(img),
    m_parent(parent),
    m_children()
{}

Quadtree::~Quadtree()
{
    m_parent = nullptr;

    m_children.clear();
}

Quadtree *Quadtree::parent() const
{
    return m_parent;
}

std::vector<Quadtree *> Quadtree::children() const
{
    return m_children;
}

int Quadtree::depth() const
{
    int ret = 0;

    const Quadtree *myParent = parent();

    while (myParent) {
        ret++;

        myParent = myParent->parent();
    }

    return ret;
}

int Quadtree::y() const
{
    return m_y;
}

int Quadtree::x() const
{
    return m_x;
}

Image Quadtree::image() const
{
    return m_image;
}

void Quadtree::split(int depth)
{
    if (!m_children.empty()) {
        return;
    }

    if (!depth || depth < -1) {
        return;
    }

    const std::array<Image, 4> childrenParts = divideImage();

    for (int i = 0; i < childrenParts.size(); ++i) {
        const Image part = childrenParts[i];

        const int w_2 = m_image.width() / 2;
        const int h_2 = m_image.height() / 2;

        int x = m_x;
        int y = m_y;

        if (i == 1) {
            x = m_x + w_2 - 1;
        }
        else if (i == 2) {
            y = m_y + h_2 - 1;
        }
        else if (i == 3) {
            x = m_x + w_2 - 1;
            y = m_y + h_2 - 1;
        }

        auto child = new Quadtree(part, x, y, this);

        m_children.push_back(child);
    }

    const int newDepth = (depth == -1) ? depth : depth - 1;

    for (Quadtree *q : m_children) {
        const Image part = q->image();

        if (part.width() > 4 && part.height() > 4) {
            q->split(newDepth);
        }
    }
}

void Quadtree::split(const Quadtree::AdaptativeFunction &func)
{
    if (!m_children.empty()) {
        return;
    }

    if (!func(this)) {
        return;
    }

    const std::array<Image, 4> childrenParts = divideImage();

    for (int i = 0; i < childrenParts.size(); ++i) {
        const Image part = childrenParts[i];

        const int partWidth = part.width();
        const int partHeight = part.height();

        int x = m_x;
        int y = m_y;

        if (i == 1) {
            x = m_x + partWidth - 1;
        }
        else if (i == 2) {
            y = m_y + partHeight - 1;
        }
        else if (i == 3) {
            x = m_x + partWidth - 1;
            y = m_y + partHeight - 1;
        }

        auto child = new Quadtree(part, x, y, this);

        m_children.push_back(child);
    }

    for (Quadtree *q : m_children) {
        const Image part = q->image();

        if (part.width() > 4 && part.height() > 4) {
            q->split(func);
        }
    }
}

std::array<Image, 4> Quadtree::divideImage() const
{
    const int w_2 = m_image.width() / 2;
    const int h_2 = m_image.height() / 2;

    std::array<Image, 4> ret = {
        m_image.subImage(0, 0, w_2, h_2),
        m_image.subImage(w_2 - 1, 0, w_2, h_2),
        m_image.subImage(0, h_2 - 1, w_2, h_2),
        m_image.subImage(w_2 - 1, h_2 - 1, w_2, h_2)
    };

    return ret;
}

} // namespace epwdip
