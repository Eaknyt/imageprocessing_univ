#ifndef H_EPWDIP_RANGEPROFILER_H
#define H_EPWDIP_RANGEPROFILER_H

#include <vector>


namespace epwdip {

class Image;

std::vector<int> profileRow(const Image &img, int row);
std::vector<int> profileCol(const Image &img, int col);

} // namespace epwdip

#endif // H_EPWDIP_RANGEPROFILER_H
