#include "stat.h"

#include <assert.h>
#include <cmath>

#include "image.h"
#include "imagequadtree.h"

namespace epwdip {

int grayscaleAvg(const Image &img)
{
    assert (img.format() == Image::Format::Grayscale8Format);

    int sum = 0;

    for (int y = 0; y < img.height(); ++y) {
        for (int x = 0; x < img.width(); ++x) {
            sum += img(x, y);
        }
    }

    const int ret = sum / img.pixelCount();

    assert (ret <= 255);

    return ret;
}

float grayscaleVariance(const Image &img)
{
    assert (img.format() == Image::Format::Grayscale8Format);

    const int gAvg = grayscaleAvg(img);

    int sum = 0;

    for (int y = 0; y < img.height(); ++y) {
        for (int x = 0; x < img.width(); ++x) {
            sum += std::pow(img(x, y) - gAvg, 2);
        }
    }

    const float ret = (1. / img.pixelCount()) * sum;

    return ret;
}

Image grayscaleAvgImage(const Image &img)
{
    // Compute avgs
    Quadtree quadtree(img, 0, 0, nullptr);
    quadtree.split(1);

    const std::vector<Quadtree *> quadtreeChildren = quadtree.children();

    std::array<int, 4> avgs = {
        grayscaleAvg(quadtreeChildren[0]->image()),
        grayscaleAvg(quadtreeChildren[1]->image()),
        grayscaleAvg(quadtreeChildren[2]->image()),
        grayscaleAvg(quadtreeChildren[3]->image())
    };

    // Compose output image
    const int w = img.width();
    const int h = img.height();

    Image ret(w, h, Image::Format::Grayscale8Format);

    const int w_2 = img.width() / 2;
    const int h_2 = img.height() / 2;

    for (int y = 0; y < h_2; ++y) {
        for (int x = 0; x < w_2; ++x) {
            ret(x, y) = avgs[0];
        }
    }

    for (int y = h_2 - 1; y < h_2 + h_2 - 1; ++y) {
        for (int x = 0; x < w_2; ++x) {
            ret(x, y) = avgs[1];
        }
    }

    for (int y = 0; y < h_2; ++y) {
        for (int x = w_2 - 1; x < w_2 + w_2 - 1; ++x) {
            ret(x, y) = avgs[2];
        }
    }

    for (int y = h_2 - 1; y < h_2 + h_2 - 1; ++y) {
        for (int x = w_2 - 1; x < w_2 + w_2 - 1; ++x) {
            ret(x, y) = avgs[3];
        }
    }

    return ret;
}

} // namespace epwdip
