#include "thresholds.h"

#include "channels.h"


namespace {

using namespace epwdip;


void threshold_1_impl(Image &img, int level)
{
    for(int y = 0; y < img.height(); ++y) {
        for(int x = 0; x < img.width(); ++x) {
            img(x, y) = (img(x, y) < level) ? 0 : 255;
        }
    }
}

void threshold_2_impl(Image &img, int level1, int level2)
{
    for(int y = 0; y < img.height(); ++y) {
        for(int x = 0; x < img.width(); ++x) {
            const unsigned char xyValue = img(x, y);

            unsigned char newValue = 255;

            if (xyValue < level1) {
                newValue = 0;
            }
            else if (xyValue < level2) {
                newValue = 128;
            }

            img(x, y) = newValue;
        }
    }
}

void threshold_3_impl(Image &img, int level1, int level2, int level3)
{
    for(int y = 0; y < img.height(); ++y) {
        for(int x = 0; x < img.width(); ++x) {
            const unsigned char xyValue = img(x, y);

            unsigned char newValue = 255;

            if (xyValue < level1) {
                newValue = 0;
            }
            else if (xyValue < level2) {
                newValue = 85;
            }
            else if (xyValue < level3) {
                newValue = 170;
            }

            img(x, y) = newValue;
        }
    }
}

} // anon namespace


namespace epwdip {

Image thresholded(const Image &img, int level)
{
    Image ret = img;

    if (img.format() != Image::Format::Rgb888Format) {
        ret = img.convert(Image::Format::BinaryFormat);

        threshold_1_impl(ret, level);
    }
    else {
        Image oRedChannel = channel(img, Channel::RedChannel);
        threshold_1_impl(oRedChannel, level);

        Image oGreenChannel = channel(img, Channel::GreenChannel);
        threshold_1_impl(oGreenChannel, level);

        Image oBlueChannel = channel(img, Channel::BlueChannel);
        threshold_1_impl(oBlueChannel, level);

        ret = assemble(oRedChannel, oGreenChannel, oBlueChannel);
    }

    return ret;
}

Image thresholded(const Image &img, int level1, int level2)
{
    Image ret = img;

    if (img.format() != Image::Format::Rgb888Format) {
        threshold_2_impl(ret, level1, level2);
    }
    else {
        Image oRedChannel = channel(img, Channel::RedChannel);
        threshold_2_impl(oRedChannel, level1, level2);

        Image oGreenChannel = channel(img, Channel::GreenChannel);
        threshold_2_impl(oGreenChannel, level1, level2);

        Image oBlueChannel = channel(img, Channel::BlueChannel);
        threshold_2_impl(oBlueChannel, level1, level2);

        ret = assemble(oRedChannel, oGreenChannel, oBlueChannel);
    }

    return ret;
}

Image thresholded(const Image &img, int level1, int level2, int level3)
{
    Image ret = img;

    if (img.format() != Image::Format::Rgb888Format) {
        threshold_3_impl(ret, level1, level2, level3);
    }
    else {
        Image oRedChannel = channel(img, Channel::RedChannel);
        threshold_3_impl(oRedChannel, level1, level2, level3);

        Image oGreenChannel = channel(img, Channel::GreenChannel);
        threshold_3_impl(oGreenChannel, level1, level2, level3);

        Image oBlueChannel = channel(img, Channel::BlueChannel);
        threshold_3_impl(oBlueChannel, level1, level2, level3);

        ret = assemble(oRedChannel, oGreenChannel, oBlueChannel);
    }

    return ret;
}

} // namespace epwdip
