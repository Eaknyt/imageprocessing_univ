#include "morpho.h"

#include <algorithm>
#include <iostream>

#include "channels.h"


namespace {

using namespace epwdip;

Image erode_binary(const Image &img)
{
    Image ret = img;

    for (int y = 1; y < img.height() - 1; ++y) {
        for (int x = 1; x < img.width() - 1; ++x) {
            ret(x, y) = img(x-1, y-1) & img(x-1, y) & img(x-1, y+1)
                    & img(x, y-1) & img(x, y) & img(x, y+1)
                    & img(x+1, y-1) & img(x+1, y) & img(x+1, y+1) & 255;
        }
    }

    return ret;
}

Image dilate_binary(const Image &img)
{
    Image ret = img;

    for (int y = 1; y < img.height() - 1; ++y) {
        for (int x = 1; x < img.width() - 1; ++x) {
            ret(x, y) = img(x-1, y-1) | img(x-1, y) | img(x-1, y+1)
                    | img(x, y-1) | img(x, y) | img(x, y+1)
                    | img(x+1, y-1) | img(x+1, y) | img(x+1, y+1) | 0;
        }
    }

    return ret;
}

Image erode_grayscale(const Image &img)
{
    Image ret = img;

    for (int y = 1; y < img.height() - 1; ++y) {
        for (int x = 1; x < img.width() - 1; ++x) {
            auto neighborhood = {
                img(x-1, y-1),  img(x-1, y),    img(x-1, y+1),
                img(x, y-1),    img(x, y),      img(x, y+1),
                img(x+1, y-1),  img(x+1, y),    img(x+1, y+1)
            };

            ret(x, y) = std::min(neighborhood);
        }
    }

    return ret;
}

Image dilate_grayscale(const Image &img)
{
    Image ret = img;

    for (int y = 1; y < img.height() - 1; ++y) {
        for (int x = 1; x < img.width() - 1; ++x) {
            auto neighborhood = {
                img(x-1, y-1),  img(x-1, y),    img(x-1, y+1),
                img(x, y-1),    img(x, y),      img(x, y+1),
                img(x+1, y-1),  img(x+1, y),    img(x+1, y+1)
            };

            ret(x, y) = std::max(neighborhood);
        }
    }

    return ret;
}

} // anon namespace


namespace epwdip {

Image erode(const Image &img)
{
    Image ret;

    switch (img.format()) {
    case Image::Format::BinaryFormat:
        ret = erode_binary(img);
        break;
    case Image::Format::Grayscale8Format:
        ret = erode_grayscale(img);
        break;
    case Image::Format::Rgb888Format:
    {
        Image rChannel = erode_grayscale(channel(img, Channel::RedChannel));
        Image gChannel = erode_grayscale(channel(img, Channel::GreenChannel));
        Image bChannel = erode_grayscale(channel(img, Channel::BlueChannel));

        ret = assemble(rChannel, gChannel, bChannel);

        break;
    }
    default:
        break;
    }

    return ret;
}

Image dilate(const Image &img)
{
    Image ret;

    switch (img.format()) {
    case Image::Format::BinaryFormat:
        ret = dilate_binary(img);
        break;
    case Image::Format::Grayscale8Format:
        ret = dilate_grayscale(img);
        break;
    case Image::Format::Rgb888Format:
    {
        Image rChannel = dilate_grayscale(channel(img, Channel::RedChannel));
        Image gChannel = dilate_grayscale(channel(img, Channel::GreenChannel));
        Image bChannel = dilate_grayscale(channel(img, Channel::BlueChannel));

        ret = assemble(rChannel, gChannel, bChannel);

        break;
    }
    default:
        break;
    }

    return ret;
}

Image difference(const Image &img1, const Image &img2)
{
    int w = img1.width();
    int h = img1.height();

    if (img1.format() != Image::Format::Grayscale8Format ||
            img2.format() != Image::Format::Grayscale8Format) {
        std::cout << "difference() > the images must be grayscale."
                  << std::endl;
        return Image();
    }

    if (w != img2.width() || h != img2.height()) {
        std::cout << "difference() > the images must have the same dimensions."
                  << std::endl;
        return Image();
    }

    Image ret(w, h, Image::Format::Grayscale8Format);

    for (int y = 0; y < img1.height(); ++y) {
        for (int x = 0; x < img1.width(); ++x) {
            unsigned char p1 = img1(x, y);
            unsigned char p2 = img2(x, y);

            if (p1 == 255 && p2 == 255) {
                ret(x, y) = 255;
            }

            if (p1 == 0 && p2 == 0) {
                ret(x, y) = 255;
            }

            if (p1 == 255 && p2 == 0) {
                ret(x, y) = 0;
            }
        }
    }

    return ret;
}

} // namespace epwdip
