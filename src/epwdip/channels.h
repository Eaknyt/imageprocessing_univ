#ifndef H_EPWDIP_CHANNEL_H
#define H_EPWDIP_CHANNEL_H

#include "epwdip_global.h"
#include "image.h"

namespace epwdip {

Image channel(const Image &img, Channel channel);

Image channels(const Image &img, Channel c1, Channel c2);

Image assemble(const Image &imgR, const Image &imgG, const Image &imgB);

} // namespace epwdip

#endif // H_EPWDIP_CHANNEL_H
