#ifndef H_EPWDIP_STAT_H
#define H_EPWDIP_STAT_H

namespace epwdip {

class Image;

int grayscaleAvg(const Image &img);

float grayscaleVariance(const Image &img);

Image grayscaleAvgImage(const Image &img);

} // namespace epwdip

#endif // H_EPWDIP_STAT_H
