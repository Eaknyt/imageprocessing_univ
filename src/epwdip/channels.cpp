#include "channels.h"

#include <assert.h>
#include <iostream>


namespace {

void redChannel(unsigned char *pt_image, const unsigned char *src, int taille_image){
    for (int i=0; i<taille_image; ++i){
        pt_image[i]=src[3*i];
    }
}

void greenChannel(unsigned char *pt_image, const unsigned char *src, int taille_image){
    for (int i=0; i<taille_image; ++i){
        pt_image[i]=src[3*i+1];
    }
}

void blueChannel(unsigned char *pt_image, const unsigned char *src, int taille_image){
    for (int i=0; i<taille_image; ++i){
        pt_image[i]=src[3*i+2];
    }
}

} // anon namespace


namespace epwdip {

Image channel(const Image &img, Channel channel)
{
    if (!img.isValid() || img.format() != Image::Format::Rgb888Format) {
        return Image();
    }

    Image ret(img.width(), img.height(), Image::Format::Grayscale8Format);

    switch(channel) {
    case Channel::RedChannel:
        redChannel(ret.data(), img.data(), ret.pixelCount());
        break;
    case Channel::GreenChannel:
        greenChannel(ret.data(), img.data(), ret.pixelCount());
        break;
    case Channel::BlueChannel:
        blueChannel(ret.data(), img.data(), ret.pixelCount());
        break;
    }

    return ret;
}

Image channels(const Image &img, Channel c1, Channel c2)
{
    if (!img.isValid() || img.format() != Image::Format::Rgb888Format) {
        return Image();
    }

    if (c1 == c2) {
        return Image();
    }

    int w = img.width();
    int h = img.height();
    const int finalImgSize = w * h * 3;

    const unsigned char *c1Data = channel(img, c1).data();
    const unsigned char *c2Data = channel(img, c2).data();

    std::vector<unsigned char> outputImgData(finalImgSize);

    int idx0 = static_cast<int>(c1);
    int idx1 = static_cast<int>(c2);
    int idx2 = 3 - idx0 - idx1;

    for (int i = 0; i < finalImgSize; i += 3) {
        int j = i / 3;

        outputImgData[i + idx0] = c1Data[j];
        outputImgData[i + idx1] = c2Data[j];
        outputImgData[i + idx2] = 0;
    }

    Image ret(outputImgData, img.width(), img.height(),
              Image::Format::Rgb888Format);

    return ret;
}

Image assemble(const Image &imgR, const Image &imgG, const Image &imgB)
{
    assert (imgR.width() == imgG.width() && imgG.width() == imgB.width());
    assert (imgR.height() == imgG.height() && imgG.height() == imgB.height());

    int w = imgR.width();
    int h = imgR.height();
    const int finalImgSize = w * h * 3;

    const unsigned char *oChannelData1 = imgR.data();
    const unsigned char *oChannelData2 = imgG.data();
    const unsigned char *oChannelData3= imgB.data();

    std::vector<unsigned char> outputImgData(finalImgSize);

    for (int i = 0; i < finalImgSize; i += 3) {
        int j = i / 3;

        outputImgData[i] = oChannelData1[j];
        outputImgData[i + 1] = oChannelData2[j];
        outputImgData[i + 2] = oChannelData3[j];
    }

    Image ret(outputImgData, w, h, Image::Format::Rgb888Format);

    assert (ret.size() == finalImgSize);

    return ret;
}

} // namespace epwdip
