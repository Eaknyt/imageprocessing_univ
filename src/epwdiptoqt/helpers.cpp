#include "helpers.h"


namespace {

using namespace epwdip;

QImage::Format imageFormatToQImageFormat(Image::Format f)
{
    QImage::Format ret = QImage::Format_Invalid;

    switch (f) {
    case Image::Format::BinaryFormat:
    case Image::Format::Grayscale8Format:
        ret = QImage::Format_Grayscale8;
        break;
    case Image::Format::Rgb888Format:
        ret = QImage::Format_RGB888;
        break;
    }

    return ret;
}

Image::Format qImageFormatToImageFormat(QImage::Format f)
{
    Image::Format ret = Image::Format::BinaryFormat;

    switch (f) {
    case QImage::Format_Grayscale8:
        ret = Image::Format::Grayscale8Format;
        break;
    case QImage::Format_RGB888:
        ret = Image::Format::Rgb888Format;
        break;
    }

    return ret;
}

} // anon namespace


namespace epwdip {

QImage image_to_qimage(const Image &img)
{
    QImage ret(img.data(), img.width(), img.height(),
               imageFormatToQImageFormat(img.format()));

    return ret;
}

Image qimage_to_image(const QImage &img)
{
    Image ret(img.constBits(), img.width(), img.height(),
              qImageFormatToImageFormat(img.format()));

    return ret;
}

} // namespace epwdip
