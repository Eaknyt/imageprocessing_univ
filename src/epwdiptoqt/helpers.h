#ifndef H_EPWDIPTOQT_HELPERS_H
#define H_EPWDIPTOQT_HELPERS_H

#include <QtGui/QImage>

#include "epwdip/image.h"

namespace epwdip {

QImage image_to_qimage(const Image &img);
Image qimage_to_image(const QImage &img);

} // namespace epwdip

#endif // H_EPWDIPTOQT_HELPERS_H
