#ifndef H_DRAWHELPERS_H
#define H_DRAWHELPERS_H

#include <QImage>

QImage opacityCheckerboard(const QRect &rect, int squareSide);

#endif // H_DRAWHELPERS_H
