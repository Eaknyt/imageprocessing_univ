#ifndef H_ADVSLIDER_H
#define H_ADVSLIDER_H

#include <QWidget>

class QLabel;
class QSlider;

class AdvSlider : public QWidget
{
    Q_OBJECT

    Q_PROPERTY(int value READ value WRITE setValue NOTIFY valueChanged)

public:
    explicit AdvSlider(QWidget *parent = nullptr);

    int value() const;
    void setValue(int val);

Q_SIGNALS:
    void valueChanged(int value);

private:
    QSlider *m_slider;
    QLabel *m_valueLabel;
};

#endif // H_ADVSLIDER_H
