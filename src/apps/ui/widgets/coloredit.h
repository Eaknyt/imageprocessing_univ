#ifndef H_COLOREDIT_H
#define H_COLOREDIT_H

#include <QtGui/QColor>

#include <QtWidgets/QFrame>


class HueSlider;
class AlphaSlider;
class QSpinBox;
class SaturationSlider;
class ValueSlider;


class ColorEdit : public QFrame
{
    Q_OBJECT

    Q_PROPERTY(QColor color READ color WRITE setColor NOTIFY colorChanged)

public:
    ColorEdit(QWidget *parent = nullptr);

    QColor color() const;

Q_SIGNALS:
    void colorChanged(const QColor &color);

public Q_SLOTS:
    void setColor(const QColor &color);

private:
    void onHueChanged(int hue);
    void onSaturationChanged(int sat);
    void onValueChanged(int value);
    void onAlphaChanged(int alpha);

private:
    QColor m_color;

    HueSlider *m_hueSlider;
    QSpinBox *m_hueSpinBox;

    SaturationSlider *m_satSlider;
    QSpinBox *m_satSpinBox;

    ValueSlider *m_valueSlider;
    QSpinBox *m_valueSpinBox;

    AlphaSlider *m_alphaSlider;
    QSpinBox *m_alphaSpinBox;
};

#endif // H_COLOREDIT_H
