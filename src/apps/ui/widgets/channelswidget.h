#ifndef H_CHANNELWIDGET_H
#define H_CHANNELWIDGET_H

#include <QWidget>


class QCheckBox;
class QLabel;

namespace epwdip {
class Image;
}

class ChannelsWidget;


class ChannelRowWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ChannelRowWidget(const QString &name,
                              ChannelsWidget *master);

    bool isChannelEnabled() const;

public Q_SLOTS:
    void setChannelEnabled(bool enable);
    void setPreview(const QImage &img);

private:
    ChannelsWidget *m_master;

    QCheckBox *m_enableCheckBox;
    QLabel *m_previewLabel;
    QLabel *m_channelLabel;
};


class ChannelsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ChannelsWidget(QWidget *parent = nullptr);

    QList<int> enabledChannels() const;

Q_SIGNALS:
    void channelEnabled(const QList<int>);

public Q_SLOTS:
    void setImage(const epwdip::Image &img);

private:
    friend class ChannelRowWidget;
    void channelRowEnabled();

private:
    QVector<ChannelRowWidget *> m_channelWidgets;
};

#endif // H_CHANNELWIDGET_H
