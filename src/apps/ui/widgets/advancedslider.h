#ifndef ADVANDEDSLIDER_H
#define ADVANDEDSLIDER_H

#include <QSlider>

class AdvancedSlider : public QSlider
{
    Q_OBJECT

public:
    AdvancedSlider(QWidget *parent = nullptr);

    void setValueAtomic(int newValue);

protected:
    void paintEvent(QPaintEvent *e) override;

    void mousePressEvent(QMouseEvent *e) override;

    virtual void drawBackground(QPainter *painter, const QRect &rect, int radius) const;
    virtual void drawHandleBackground(QPainter *painter, const QRect &rect, int radius) const;
};

#endif // ADVANDEDSLIDER_H
