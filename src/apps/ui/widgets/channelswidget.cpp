#include "channelswidget.h"

#include <QCheckBox>
#include <QHBoxLayout>
#include <QLabel>

#include "epwdip/channels.h"
#include "epwdip/image.h"

#include "epwdiptoqt/helpers.h"


using namespace epwdip;


////////////////////// ChannelRowWidget //////////////////////

ChannelRowWidget::ChannelRowWidget(const QString &name, ChannelsWidget *master) :
    QWidget(master),
    m_master(master),
    m_enableCheckBox(new QCheckBox(this)),
    m_previewLabel(new QLabel(this)),
    m_channelLabel(new QLabel(name, this))
{
    auto layout = new QHBoxLayout(this);

    m_previewLabel->setFixedSize(64, 64);
    m_enableCheckBox->setChecked(true);

    layout->addWidget(m_enableCheckBox);
    layout->addWidget(m_previewLabel);
    layout->addWidget(m_channelLabel);
    layout->addStretch();

    connect(m_enableCheckBox, &QCheckBox::toggled,
            this, &ChannelRowWidget::setChannelEnabled);
}

bool ChannelRowWidget::isChannelEnabled() const
{
    return m_enableCheckBox->isChecked();
}

void ChannelRowWidget::setChannelEnabled(bool enable)
{
    if (m_enableCheckBox->isChecked() != enable) {
        QSignalBlocker blocker(m_enableCheckBox);

        m_enableCheckBox->setChecked(enable);
    }

    m_previewLabel->setEnabled(enable);
    m_channelLabel->setEnabled(enable);

    m_master->channelRowEnabled();
}

void ChannelRowWidget::setPreview(const QImage &img)
{
    QPixmap pm = QPixmap::fromImage(img);

    if (pm.isNull()) {
        m_previewLabel->clear();
    }
    else {
        m_previewLabel->setPixmap(pm.scaled(64, 64, Qt::KeepAspectRatio));
    }
}


////////////////////// ChannelsWidget //////////////////////

ChannelsWidget::ChannelsWidget(QWidget *parent) :
    QWidget(parent),
    m_channelWidgets(3)
{
    m_channelWidgets[0] = new ChannelRowWidget("      Red", this);
    m_channelWidgets[1] = new ChannelRowWidget("      Green", this);
    m_channelWidgets[2] = new ChannelRowWidget("      Blue", this);

    auto layout = new QVBoxLayout(this);
    layout->addWidget(m_channelWidgets[0]);
    layout->addWidget(m_channelWidgets[1]);
    layout->addWidget(m_channelWidgets[2]);
}

QList<int> ChannelsWidget::enabledChannels() const
{
    QList<int> ret;

    for (int i = 0; i < m_channelWidgets.size(); ++i) {
        ChannelRowWidget *w = m_channelWidgets[i];

        if (w->isChannelEnabled()) {
            ret.push_back(i);
        }
    }

    return ret;
}

void ChannelsWidget::setImage(const epwdip::Image &img)
{
    bool isColored = (img.format() != Image::Format::Grayscale8Format);

    for (ChannelRowWidget *w : m_channelWidgets) {
        w->setVisible(isColored);
    }

    if (m_channelWidgets[0]->isVisible()) {
        m_channelWidgets[0]->setPreview(image_to_qimage(channel(img, Channel::RedChannel)));
        m_channelWidgets[1]->setPreview(image_to_qimage(channel(img, Channel::GreenChannel)));
        m_channelWidgets[2]->setPreview(image_to_qimage(channel(img, Channel::BlueChannel)));
    }
}

void ChannelsWidget::channelRowEnabled()
{
    QList<int> channels = enabledChannels();

    Q_EMIT channelEnabled(channels);
}
