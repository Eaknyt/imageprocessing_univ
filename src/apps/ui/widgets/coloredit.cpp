#include "coloredit.h"

#include <QtWidgets/QSpinBox>
#include <QtWidgets/QVBoxLayout>

#include "hueslider.h"
#include "alphaslider.h"
#include "saturationslider.h"
#include "valueslider.h"


ColorEdit::ColorEdit(QWidget *parent) :
    QFrame(parent),
    m_color(),
    m_hueSlider(new HueSlider(this)),
    m_hueSpinBox(new QSpinBox(this)),
    m_satSlider(new SaturationSlider(this)),
    m_satSpinBox(new QSpinBox(this)),
    m_valueSlider(new ValueSlider(this)),
    m_valueSpinBox(new QSpinBox(this)),
    m_alphaSlider(new AlphaSlider(this)),
    m_alphaSpinBox(new QSpinBox(this))
{
    // Hue widgets
    m_hueSlider->setOrientation(Qt::Horizontal);

    connect(m_hueSlider, &HueSlider::valueChanged,
            this, &ColorEdit::onHueChanged);

    m_hueSpinBox->setMaximum(359);

    connect(m_hueSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &ColorEdit::onHueChanged);

    // Saturation widgets
    m_satSlider->setOrientation(Qt::Horizontal);

    connect(m_satSlider, &SaturationSlider::valueChanged,
            this, &ColorEdit::onSaturationChanged);

    m_satSpinBox->setMaximum(255);

    connect(m_satSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &ColorEdit::onSaturationChanged);

    // Value widgets
    m_valueSlider->setOrientation(Qt::Horizontal);

    connect(m_valueSlider, &ValueSlider::valueChanged,
            this, &ColorEdit::onValueChanged);

    m_valueSpinBox->setMaximum(255);

    connect(m_valueSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &ColorEdit::onValueChanged);

    // Alpha widgets
    m_alphaSlider->setOrientation(Qt::Horizontal);

    connect(m_alphaSlider, &AlphaSlider::valueChanged,
            this, &ColorEdit::onAlphaChanged);

    m_alphaSpinBox->setMaximum(255);

    connect(m_alphaSpinBox, static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
            this, &ColorEdit::onAlphaChanged);

    // Assemble
    auto myLayout = new QGridLayout(this);
    myLayout->setContentsMargins(0, 0, 0, 0);

    myLayout->addWidget(m_hueSlider, 0, 0);
    myLayout->addWidget(m_hueSpinBox, 0, 1);
    myLayout->addWidget(m_satSlider, 1, 0);
    myLayout->addWidget(m_satSpinBox, 1, 1);
    myLayout->addWidget(m_valueSlider, 2, 0);
    myLayout->addWidget(m_valueSpinBox, 2, 1);
    myLayout->addWidget(m_alphaSlider, 3, 0);
    myLayout->addWidget(m_alphaSpinBox, 3, 1);
}

QColor ColorEdit::color() const
{
    return m_color;
}

void ColorEdit::setColor(const QColor &color)
{
    if (m_color != color) {
        m_color = color;

        const int newHue = m_color.hsvHue();
        const int newSat = m_color.hsvSaturation();
        const int newValue = m_color.value();
        const int newAlpha = m_color.alpha();

        m_hueSlider->setValueAtomic(newHue);

        m_hueSpinBox->setValue(newHue);

        m_satSlider->setHue(newHue);
        m_satSlider->setValueAtomic(m_color.hsvSaturation());

        m_satSpinBox->setValue(newSat);

        m_valueSlider->setValueAtomic(m_color.value());
        m_valueSlider->setHue(newHue);

        m_valueSpinBox->setValue(newValue);

        m_alphaSlider->setValueAtomic(newAlpha);
        m_alphaSlider->setHsv(newHue, newSat, newValue);

        m_alphaSpinBox->setValue(newAlpha);

        Q_EMIT colorChanged(color);
    }
}

void ColorEdit::onHueChanged(int hue)
{
    QColor newColor = QColor::fromHsv(hue,
                                      m_color.hsvSaturation(),
                                      m_color.value(),
                                      m_color.alpha());

    setColor(newColor);
}

void ColorEdit::onSaturationChanged(int sat)
{
    QColor newColor = QColor::fromHsv(m_color.hsvHue(),
                                      sat,
                                      m_color.value(),
                                      m_color.alpha());

    setColor(newColor);
}

void ColorEdit::onValueChanged(int value)
{
        QColor newColor = QColor::fromHsv(m_color.hsvHue(),
                                          m_color.hsvSaturation(),
                                          value,
                                          m_color.alpha());

        setColor(newColor);
}

void ColorEdit::onAlphaChanged(int alpha)
{
    QColor newColor = QColor::fromHsv(m_color.hsvHue(),
                                      m_color.hsvSaturation(),
                                      m_color.value(),
                                      alpha);

    setColor(newColor);
}
