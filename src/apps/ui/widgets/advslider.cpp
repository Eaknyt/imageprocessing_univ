#include "advslider.h"

#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSlider>

AdvSlider::AdvSlider(QWidget *parent) :
    QWidget(parent),
    m_slider(new QSlider(Qt::Horizontal, this)),
    m_valueLabel(new QLabel(this))
{
    auto layout = new QHBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    m_slider->setMinimum(0);
    m_slider->setMaximum(255);

    m_valueLabel->setFixedWidth(fontMetrics().width("555"));
    m_valueLabel->setText(QString::number(m_slider->value()));

    connect(m_slider, &QSlider::valueChanged,
            [=] (int val) {
        m_valueLabel->setText(QString::number(val));

        Q_EMIT valueChanged(val);
    });

    layout->addWidget(m_slider);
    layout->addWidget(m_valueLabel);
}

int AdvSlider::value() const
{
    return m_slider->value();
}

void AdvSlider::setValue(int val)
{
    if (value() != val) {
        m_slider->setValue(val);

        Q_EMIT valueChanged(val);
    }
}
