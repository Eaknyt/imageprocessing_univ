#include "alphaslider.h"

#include <QtGui/QPainter>
#include <QtGui/QResizeEvent>
#include <QDebug>
#include "drawhelpers.h"


////////////////////////// AlphaSliderImpl //////////////////////////

class AlphaSliderImpl
{
public:
    AlphaSliderImpl();

    /* functions */
    void updateCheckerboard(const QRect &rect);

    /* variables */
    int h, s, v;
    QBrush checkerboard;
};

AlphaSliderImpl::AlphaSliderImpl() :
    h(0),
    s(0),
    v(0),
    checkerboard()
{}

void AlphaSliderImpl::updateCheckerboard(const QRect &rect)
{
    checkerboard = opacityCheckerboard(rect, 3);
}


//////////////////////////// AlphaSlider /////////////////////////////

AlphaSlider::AlphaSlider(QWidget *parent)
    : AdvancedSlider(parent),
      d(new AlphaSliderImpl)
{
    setRange(0, 255);
    setValue(255);
}

AlphaSlider::~AlphaSlider()
{}

void AlphaSlider::hsv(int *h, int *s, int *v) const
{
    *h = d->h;
    *s = d->s;
    *v = d->v;
}

void AlphaSlider::setHsv(int h, int s, int v)
{
    bool updateImage = false;

    if (d->h != h) {
        d->h = h;
        updateImage = true;
    }

    if (d->s != s) {
        d->s = s;
        updateImage = true;
    }

    if (d->v != v) {
        d->v = v;
        updateImage = true;
    }

    if (updateImage) {
        update();
    }
}

void AlphaSlider::resizeEvent(QResizeEvent *e)
{
    if (e->size().isEmpty()) {
        return;
    }

    d->updateCheckerboard(rect());
}

void AlphaSlider::drawBackground(QPainter *painter, const QRect &rect, int radius) const
{
    painter->setBrush(d->checkerboard);
    painter->drawRoundedRect(rect, radius, radius);

    painter->setPen(QPen(Qt::black, 0.5));

    QLinearGradient gradient(QPoint(0.0, 0.0), QPoint(width(), height()));
    QColor gradientColor = QColor::fromHsv(d->h, d->s, d->v);

    const bool isVertical = (orientation() == Qt::Vertical);

    gradientColor.setAlphaF(isVertical ? 1.0 : 0.0);
    gradient.setColorAt(0.0, gradientColor);

    gradientColor.setAlphaF(isVertical ? 0.0 : 1.0);
    gradient.setColorAt(1.0, gradientColor);

    painter->setBrush(gradient);
    painter->drawRoundedRect(rect, radius, radius);
}

void AlphaSlider::drawHandleBackground(QPainter *painter, const QRect &rect, int radius) const
{
    painter->setBrush(d->checkerboard);
    painter->drawRoundedRect(rect, radius, radius);

    painter->setBrush(QColor::fromHsv(d->h, d->s, d->v, value()));
    painter->drawRoundedRect(rect, radius, radius);
}
