#ifndef ALPHASLIDER_H
#define ALPHASLIDER_H

#include "advancedslider.h"


class AlphaSliderImpl;

class AlphaSlider : public AdvancedSlider
{
    Q_OBJECT

public:
    explicit AlphaSlider(QWidget *parent = nullptr);
    ~AlphaSlider();

    void hsv(int *h, int *s, int *v) const;
    void setHsv(int h, int s, int v);

protected:
    void resizeEvent(QResizeEvent *) override;

    void drawBackground(QPainter *painter, const QRect &rect, int radius) const override;
    void drawHandleBackground(QPainter *painter, const QRect &rect, int radius) const override;

private:
    QScopedPointer<AlphaSliderImpl> d;
};

#endif // ALPHASLIDER_H
