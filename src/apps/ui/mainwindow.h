#ifndef H_MAINWINDOW_H
#define H_MAINWINDOW_H

#include <array>
#include <memory>

#include <QtWidgets/QMainWindow>

#include "epwdip/image.h"


class QImage;
class QLabel;

class AdvSlider;
class EffectsFactoriesDB;
class EffectPropertiesWidget;
class ImageNode;
class NodeGraph;
class NodeGui;
class ViewerManager;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private Q_SLOTS:
    void onNodeAdded(NodeGui *nodeGui);
    void onNodeSelectionChanged();
//    void onNodeMarkedFinal(NodeGui *nodeGui);
    void onEffectPropertyEdited(ImageNode *node);

private:
//    ImageNode *findFinalNodeForPath(ImageNode *node) const;
    ImageNode *findViewerNode(ImageNode *node) const;

    void render(ImageNode *viewerNode);

private:
    std::unique_ptr<EffectsFactoriesDB> m_effectsFactoriesDB;

    ViewerManager *m_viewerManager;
    NodeGraph *m_nodeGraph;
    EffectPropertiesWidget *m_nodePropsWidget;
};

#endif // H_MAINWINDOW_H
