#include "nodegraph.h"

#include <cmath>

#include <QtCore/QDebug>

#include <QtGui/QKeyEvent>

#include <QtWidgets/QListWidget>

#include "addnodedialog.h"
#include "effectfactoriesdatabase.h"
#include "imagenode.h"
#include "mainwindow.h"
#include "nodegui.h"

#include "effects/viewereffect.h"


namespace {

////////////////////// Helpers //////////////////////

QColor colorForNode(ImageNode *node)
{
    QColor ret;

//    const std::string effectName = node->effectName();

//    if (effectName == "Viewer") {
//        ret = Qt::yellow;
//    }
//    else if (effectName == "Channel") {
//        ret = Qt::gray;
//    }
//    else if (effectName == "Close") {
//        ret = Qt::red;
//    }
//    else if (effectName == "Dilate") {
//        ret = Qt::darkRed;
//    }
//    else if (effectName == "Empty") {
//        return Qt::white;
//    }
//    else if (effectName == "Erode") {
//        ret = Qt::magenta;
//    }
//    else if (effectName == "File") {
//        ret = Qt::lightGray;
//    }
//    else if (effectName == "Open") {
//        ret = Qt::blue;
//    }
//    else if (effectName == "Threshold") {
//        ret = Qt::darkGray;
//    }
//    else if (effectName == "Fill") {
//        ret = Qt::blue;
//    }
//    else if (effectName == "Inpaint") {
//        ret = Qt::white;
//    }

    ret = Qt::lightGray;

    Q_ASSERT (ret.isValid());

    return ret;
}

} // anon namespace


////////////////////// NodeGraph //////////////////////

NodeGraph::NodeGraph(EffectsFactoriesDB *effectFactories, QWidget *parent) :
    QGraphicsView(parent),
    m_effectFactoriesDB(effectFactories),
    m_addNodeDialog(new AddNodeDialog(effectFactories, this)),
    m_viewerNode(std::make_unique<ImageNode>()),
    m_lastAddedNodeGui(nullptr),
    m_gridOffsets()
{
    // Create the nodes dialog
    m_addNodeDialog->setVisible(false);

    connect(m_addNodeDialog, &AddNodeDialog::nodeItemActivated,
            this, &NodeGraph::onNodeItemActivated);

    // Setup the view
    setDragMode(QGraphicsView::ScrollHandDrag);
    setContextMenuPolicy(Qt::ActionsContextMenu);

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    setMouseTracking(true);

    setRenderHint(QPainter::Antialiasing);

    setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    setScene(new QGraphicsScene(0, 0, 400, 100, this));

    // Add the viewer node
//    std::unique_ptr<Effect> viewerEffect = m_effectFactoriesDB->factory("Viewer")->createEffect(m_viewerNode.get());
//    m_viewerNode->setEffect(std::move(viewerEffect));
//    addNode(std::move(m_viewerNode));

    setSceneRect(0, -100, 400, 400);
}

NodeGraph::~NodeGraph()
{}

void NodeGraph::addNode(std::unique_ptr<ImageNode> &&node)
{
    auto nodeGui = new NodeGui(std::move(node), this);
    nodeGui->setColor(colorForNode(nodeGui->node()));

    QGraphicsScene *myScene = scene();
    myScene->addItem(nodeGui);

    nodeGui->setPos((m_lastAddedNodeGui ? m_lastAddedNodeGui->x() : 0) + 100,
                    0);

    unselectAllNodes();
    nodeGui->setSelected(true);

    m_lastAddedNodeGui = nodeGui;

    Q_EMIT nodeAdded(nodeGui);
}

void NodeGraph::notifyNodeFinalMarked(NodeGui *nodeGui)
{
    Q_EMIT nodeMarkedFinal(nodeGui);
}

NodeGui *NodeGraph::nodeAt(const QPointF &pos) const
{
    auto itemsUnderMouse = scene()->items(pos, Qt::IntersectsItemBoundingRect,
                                          Qt::AscendingOrder);

    NodeGui *ret = nullptr;

    for (QGraphicsItem *item : itemsUnderMouse) {
        auto maybeNode = qgraphicsitem_cast<NodeGui *>(item);

        if (maybeNode) {
            ret = maybeNode;
            break;
        }
    }

    return ret;
}

void NodeGraph::drawBackground(QPainter *painter, const QRectF &rect)
{
    paintGrid(painter, rect);
}

void NodeGraph::showEvent(QShowEvent *)
{
    computeGridOffsets();
}

void NodeGraph::keyPressEvent(QKeyEvent *e)
{
    const int keyId = e->key();

    if (keyId == Qt::Key_Space) {
        openNodesDialog();
    }
}

void NodeGraph::onNodeItemActivated(const QString &factoryName)
{
    // Retrieve the corresponding effect factory
    const std::string factoryNameStd = factoryName.toStdString();

    AbstractEffectFactory *factory = m_effectFactoriesDB->factory(factoryNameStd);

    if (!factory) {
        qCritical() << "No effect factory registered with name" << factoryName;
        return;
    }

    // Create the node
    auto node = std::make_unique<ImageNode>();
    std::unique_ptr<Effect> effect = factory->createEffect(node.get());

    node->setEffect(std::move(effect));

    // Add the node to the graph
    addNode(std::move(node));

    // Close the dialog and retrieve focus
    m_addNodeDialog->setVisible(false);

    setFocus();
}

void NodeGraph::openNodesDialog()
{
    if (m_addNodeDialog->isVisible()) {
        return;
    }

    m_addNodeDialog->setVisible(true);
    m_addNodeDialog->setFocus();
}

void NodeGraph::unselectAllNodes()
{
    for (QGraphicsItem *item : scene()->selectedItems()) {
        item->setSelected(false);
    }
}

qreal NodeGraph::nearestPowerOf10(qreal value)
{
    qreal ret = std::pow(10, std::floor(std::log10(std::abs(value)) + 0.5));

    return (value < 0) ? -ret : ret;
}

void NodeGraph::computeGridOffsets()
{
    const QRectF rect = sceneRect();

    const qreal xOffset = nearestPowerOf10(rect.right() - rect.left()) / 10;
    const qreal yOffset = nearestPowerOf10(rect.bottom() - rect.top()) / 10;

    if (xOffset < 1 || yOffset < 1) {
        m_blockForwardWheel = true;

        return;
    }

    if (m_blockForwardWheel) {
        m_blockForwardWheel = false;
    }

    m_gridOffsets = QPointF(xOffset, yOffset);
}

void NodeGraph::paintGrid(QPainter *p, const QRectF &rect)
{
    QPen pen(Qt::DashLine);
    p->setPen(pen);

    // Lines on X
    const qreal yOffset = m_gridOffsets.y();

    for (qreal y = 0; y > rect.top(); y -= m_gridOffsets.y()) {
        paintGridLineAndTickX(p, y, rect);
    }

    for (qreal y = yOffset; y < rect.bottom(); y += yOffset) {
        paintGridLineAndTickX(p, y, rect);
    }

    // Lines on Y
    const qreal xOffset = m_gridOffsets.x();

    for (qreal x = 0; x > rect.left(); x -= xOffset) {
        printGridLineAndTickY(p, x, rect);
    }

    for (qreal x = xOffset; x < rect.right(); x += xOffset) {
        printGridLineAndTickY(p, x, rect);
    }
}

void NodeGraph::paintGridLineAndTickX(QPainter *p, qreal y, const QRectF &rect)
{
    QPointF from(rect.left(), y);
    QPointF to(rect.right(), y);

    QPen pen = p->pen();
    pen.setStyle(Qt::DashLine);
    pen.setColor(Qt::lightGray);

    p->setPen(pen);
    p->drawLine(from, to);
}

void NodeGraph::printGridLineAndTickY(QPainter *p, qreal x, const QRectF &rect)
{
    QPointF from(x, rect.bottom());
    QPointF to(x, rect.top());

    QPen pen = p->pen();
    pen.setStyle(Qt::DashLine);
    pen.setColor(Qt::lightGray);

    p->setPen(pen);
    p->drawLine(from, to);
}
