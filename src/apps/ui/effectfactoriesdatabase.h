#ifndef H_EFFECTFACTORIESDATABASE_H
#define H_EFFECTFACTORIESDATABASE_H

#include <map>
#include <memory>
#include <string>
#include <vector>

class AbstractEffectFactory;

class EffectsFactoriesDB
{
public:
    EffectsFactoriesDB();
    ~EffectsFactoriesDB();

    std::vector<std::string> factoriesNames() const;

    AbstractEffectFactory *factory(const std::string &factoryName) const;

private:
    void registerEffectFactories();
    void registerEffectFactory(std::unique_ptr<AbstractEffectFactory> &&factory);

private:
    std::map<std::string, std::unique_ptr<AbstractEffectFactory>> m_effectFactories;
};

#endif // H_EFFECTFACTORIESDATABASE_H
