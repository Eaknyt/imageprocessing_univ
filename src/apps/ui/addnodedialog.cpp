#include "addnodedialog.h"

#include <QtGui/QKeyEvent>

#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QVBoxLayout>

#include <QDebug>
#include <QApplication>

#include "effectfactoriesdatabase.h"

AddNodeDialog::AddNodeDialog(EffectsFactoriesDB *db, QWidget *parent) :
    QFrame(parent),
    m_filterLine(new QLineEdit(this)),
    m_listWidget(new QListWidget(this)),
    m_baseFactoriesNames(),
    m_filterRegex("", QRegularExpression::CaseInsensitiveOption)
{
    setFrameStyle(QFrame::Panel);

    auto layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);

    m_filterLine->setPlaceholderText("Filter...");
    m_filterLine->setFocus();

    connect(m_filterLine, &QLineEdit::textChanged,
            this, &AddNodeDialog::onFilterChanged);

    layout->addWidget(m_filterLine);
    layout->addWidget(m_listWidget);

    setFocusProxy(m_filterLine);
    setTabOrder(m_filterLine, m_listWidget);

    // Populate the list widget using factories names
    for (const std::string &factoryName : db->factoriesNames()) {
        m_baseFactoriesNames << QString::fromStdString(factoryName);
    }

    m_listWidget->addItems(m_baseFactoriesNames);

    connect(m_listWidget, &QListWidget::itemActivated,
            [=] (QListWidgetItem *item) {
        Q_EMIT nodeItemActivated(item->text());

        m_filterLine->clear();
    });
}

void AddNodeDialog::onFilterChanged(const QString &text)
{
    QStringList newItems;

    if (text.isEmpty()) {
        newItems << m_baseFactoriesNames;
    }
    else {
        static const QString letterRegex = "[A-Za-z]*";

        QString pattern(letterRegex);

        for (const QChar &c : text) {
            pattern += c + letterRegex;
        }

        m_filterRegex.setPattern(pattern);
        m_filterRegex.optimize();

        for (const QString &factoryName : m_baseFactoriesNames) {
            QRegularExpressionMatch matches = m_filterRegex.match(factoryName);

            if (matches.hasMatch()) {
                newItems << factoryName;
            }
        }
    }

    m_listWidget->clear();
    m_listWidget->addItems(newItems);
}


void AddNodeDialog::keyPressEvent(QKeyEvent *e)
{
    const int key = e->key();

    if (key == Qt::Key_Escape) {
        setVisible(false);

        // Release focus
        m_filterLine->clear();
        m_filterLine->clearFocus();

        QWidget *parentW = parentWidget();

        if (parentW) {
            parentW->setFocus();
        }
    }
}
