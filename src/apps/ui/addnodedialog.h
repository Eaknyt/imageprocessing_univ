#ifndef H_ADDNODEDIALOG_H
#define H_ADDNODEDIALOG_H

#include <QtCore/QRegularExpression>

#include <QtWidgets/QFrame>

class QLineEdit;
class QListWidget;

class EffectsFactoriesDB;

class AddNodeDialog : public QFrame
{
    Q_OBJECT

public:
    AddNodeDialog(EffectsFactoriesDB *db, QWidget *parent = nullptr);

    void keyPressEvent(QKeyEvent *e) override;

Q_SIGNALS:
    void nodeItemActivated(const QString &name);

private Q_SLOTS:
    void onFilterChanged(const QString &text);

public:
    QLineEdit *m_filterLine;
    QListWidget *m_listWidget;
    QStringList m_baseFactoriesNames;
    QRegularExpression m_filterRegex;
};

#endif // H_ADDNODEDIALOG_H
