#include "imagenode.h"

#include <iostream>

ImageNode::ImageNode() :
    m_previous(nullptr),
    m_next(nullptr),
    m_effect(nullptr),
    m_isDirty(false),
    m_isFinal(false)
{}

ImageNode::~ImageNode()
{
    if (m_previous) {
        m_previous->m_next = nullptr;
        m_previous = nullptr;
    }

    if (m_next) {
        m_next->m_previous = nullptr;
        m_next = nullptr;
    }
}

ImageNode *ImageNode::previous() const
{
    return m_previous;
}

void ImageNode::setPrevious(ImageNode *node)
{
    if (m_previous != node) {
        // Disconnect the current previous node
        if (m_previous) {
            m_previous->m_next = nullptr;
        }

        // Connect to the replacing node
        if (node) {
            node->m_next = this;
        }

        m_previous = node;
    }
}

ImageNode *ImageNode::next() const
{
    return m_next;
}

void ImageNode::setNext(ImageNode *node)
{
    if (m_next != node) {
        // Disconnect the current next node
        if (m_next) {
            m_next->m_previous = nullptr;
        }

        // Connect to the replacing node
        if (node) {
            node->m_previous = this;
        }

        m_next = node;
    }
}

bool ImageNode::isDirty() const
{
    return m_isDirty;
}

void ImageNode::setDirty(bool dirty)
{
    if (m_isDirty != dirty) {
        m_isDirty = dirty;
    }
}

bool ImageNode::isFinal() const
{
    return m_isFinal;
}

void ImageNode::setFinal()
{
    if (!m_isFinal) {
        m_isFinal = true;

        // Deactivate linked nodes
        ImageNode *prev = m_previous;

        while (prev) {
            prev->m_isFinal = false;

            prev = prev->m_previous;
        }

        ImageNode *next = m_next;

        while (next) {
            next->m_isFinal = false;

            next = next->m_next;
        }
    }
}

Effect *ImageNode::effect() const
{
    return m_effect.get();
}

void ImageNode::setEffect(std::unique_ptr<Effect> &&effect)
{
    if (m_effect != effect && effect) {
        m_effect = std::move(effect);

        setDirty(true);
    }
}

std::string ImageNode::effectName() const
{
    return m_effect->name();
}

const epwdip::Image &ImageNode::evaluate()
{
    if (!m_isDirty) {
        return m_cache;
    }

    if (m_previous) {
        m_cache = m_previous->evaluate();
    }

    if (m_next) {
        m_next->m_isDirty = true;
    }

    if (m_effect) {
        m_effect->apply(m_cache);

        m_isDirty = false;
    }

    return m_cache;
}
