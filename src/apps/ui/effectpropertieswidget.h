#ifndef H_EFFECTPROPERTIESWIDGET_H
#define H_EFFECTPROPERTIESWIDGET_H

#include <memory>

#include <QtWidgets/QWidget>

class QFormLayout;
class QFrame;
class QLabel;
class QVBoxLayout;

class EffectPropertyEditorDelegate;
class ImageNode;
class NodeGui;

class EffectPropertiesWidget : public QWidget
{
    Q_OBJECT

public:
    explicit EffectPropertiesWidget(QWidget *parent = nullptr);
    ~EffectPropertiesWidget();

    void setNodeGui(NodeGui *nodeGui);

    void updateEffect(const QString &propName, const QVariant &value);

Q_SIGNALS:
    void effectPropertyEdited(ImageNode *);

private:
    void populatePropsForm();

private:
    QFrame *m_leftSideFrame;

    QVBoxLayout *m_centralLayout;
    QLabel *m_nodeLabel;
    QWidget *m_innerPropsWidget;
    QFormLayout *m_propsFormLayout;

    std::unique_ptr<EffectPropertyEditorDelegate> m_editorDelegate;

    NodeGui *m_currentNodeGui;
};

#endif // H_EFFECTPROPERTIESWIDGET_H
