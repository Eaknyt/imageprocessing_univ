#ifndef H_HISTOWIDGET_H
#define H_HISTOWIDGET_H

#include <QtCharts/QChartView>

namespace QtCharts {
class QLineSeries;
}

namespace epwdip {
class Image;
}

class HistoWidget : public QtCharts::QChartView
{
    Q_OBJECT

public:
    HistoWidget(QWidget *parent = nullptr);

public Q_SLOTS:
    void setImage(const epwdip::Image &img);

private:
    void removeSeries();
    void updateSeries(QtCharts::QLineSeries *series,
                      const std::vector<int> hist);

private:
    QtCharts::QLineSeries *m_graySeries;

    QtCharts::QLineSeries *m_redSeries;
    QtCharts::QLineSeries *m_greenSeries;
    QtCharts::QLineSeries *m_blueSeries;

    QtCharts::QChart *m_chart;
};

#endif // H_HISTOWIDGET_H
