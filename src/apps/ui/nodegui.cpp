#include "nodegui.h"

#include <QtCore/QDebug>

#include <QtGui/QPainter>

#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsSceneEvent>
#include <QtWidgets/QStyleOptionGraphicsItem>

#include "epwdiptoqt/helpers.h"

#include "imagenode.h"
#include "nodegraph.h"


////////////////////// NodeGui::Link //////////////////////

NodeGui::Link::Link(NodeGui *output) :
    m_fromPos(),
    m_toPos(),
    m_inputNode(nullptr),
    m_outputNode(nullptr),
    m_shape(Shape::CurveShape)
{
    setOutput(output);

    setPen((QPen(Qt::black, 2)));
}

QPointF NodeGui::Link::fromPos() const
{
    return m_fromPos;
}

void NodeGui::Link::setFromPos(const QPointF &scenePos)
{
    if (m_fromPos != scenePos) {
        m_fromPos = scenePos;

        computePath();
    }
}

QPointF NodeGui::Link::toPos() const
{
    return m_toPos;
}

void NodeGui::Link::setToPos(const QPointF &scenePos)
{
    if (m_toPos != scenePos) {
        m_toPos = scenePos;

        computePath();
    }
}

NodeGui *NodeGui::Link::output() const
{
    return m_outputNode;
}

void NodeGui::Link::setOutput(NodeGui *node)
{
    if (m_outputNode != node && node) {
        m_outputNode = node;

        const QPointF newFromPos = node->outputConnectorCenterScenePos();

        setFromPos(newFromPos);
    }
}

NodeGui *NodeGui::Link::input() const
{
    return m_inputNode;
}

void NodeGui::Link::setInput(NodeGui *node)
{
    if (m_inputNode != node && node) {
        m_inputNode = node;

        const QPointF newToPos = node->inputConnectorCenterScenePos();

        setToPos(newToPos);
    }
}

void NodeGui::Link::computePath()
{
    QPainterPath path;

    path.moveTo(m_fromPos);

    switch (m_shape)
    {
    case LineShape:
        path.lineTo(m_toPos);

        break;
    case CurveShape:
        qreal dx = m_toPos.x() - m_fromPos.x();
        qreal dy = m_toPos.y() - m_fromPos.y();

        QPointF ctr1(m_fromPos.x() + dx * 0.25, m_fromPos.y() + dy * 0.1);
        QPointF ctr2(m_fromPos.x() + dx * 0.75, m_fromPos.y() + dy * 0.9);

        path.cubicTo(ctr1, ctr2, m_toPos);

        break;
    }

    setPath(path);
}

void NodeGui::Link::trackNodes()
{
    if (!m_outputNode || !m_inputNode) {
        return;
    }

    const QPointF newFromPos = m_outputNode->outputConnectorCenterScenePos();
    setFromPos(newFromPos);

    const QPointF newToPos = m_inputNode->inputConnectorCenterScenePos();
    setToPos(newToPos);
}


////////////////////// NodeGui //////////////////////

NodeGui::NodeGui(std::unique_ptr<ImageNode> &&node, NodeGraph *graph,
                 QGraphicsItem *parent) :
    QGraphicsRectItem(parent),
    m_node(std::move(node)),
    m_nodeGraph(graph),
    m_color(),
    m_eState(EventState::MoveState),
    m_outputLink(nullptr),
    m_inputLink(nullptr)
{
    setFlags(ItemIsSelectable | ItemIsMovable | ItemSendsScenePositionChanges);
    setZValue(0);
}

NodeGui::~NodeGui()
{}

int NodeGui::type() const
{
    return Type;
}

ImageNode *NodeGui::node() const
{
    return m_node.get();
}

QColor NodeGui::color() const
{
    return m_color;
}

void NodeGui::setColor(const QColor &color)
{
    if (m_color != color) {
        m_color = color;

        update();
    }
}

NodeGui::Link *NodeGui::outputLink() const
{
    return m_outputLink;
}

NodeGui::Link *NodeGui::inputLink() const
{
    return m_inputLink;
}

QRectF NodeGui::boundingRect() const
{
    return shape().boundingRect();
}

QPainterPath NodeGui::shape() const
{
    const QRectF rect = nodeRect();

    QPainterPath ret;
    ret.addRect(rect);

    const qreal connectorRadius = connectorRadii();

    ret.addEllipse(inputConnectorCenter(), connectorRadius, connectorRadius);
    ret.addEllipse(outputConnectorCenter(), connectorRadius, connectorRadius);

    return ret;
}

void NodeGui::paint(QPainter *p, const QStyleOptionGraphicsItem *option,
                    QWidget *)
{
    QPen pen(Qt::black);

    if (option->state & QStyle::State_Selected) {
        pen.setColor(Qt::white);
    }

    p->setPen(pen);
    p->setBrush(m_color);

    const QRectF nRect = nodeRect();
    p->drawRect(nRect);

    // Paint text
    p->setPen(Qt::black);

    const QString nodeTitle = QString::fromStdString(m_node->effectName());
    p->drawText(nRect, Qt::AlignHCenter, nodeTitle);

    // Paint controls
    const QRectF mrRect = markFinalRect();
    (m_node->isFinal()) ? p->fillRect(mrRect, Qt::cyan) : p->drawRect(mrRect);

    const qreal connectorRadius = connectorRadii();

    p->drawEllipse(inputConnectorCenter(), connectorRadius, connectorRadius);

    p->drawEllipse(outputConnectorCenter(), connectorRadius, connectorRadius);

    // Paint image computed by this effect
    const epwdip::Image &img = m_node->evaluate();

    if (!img.isNull()) {
        const QFontMetricsF metrics = qApp->fontMetrics();
        const qreal offsetFromTitle = metrics.boundingRect(nodeTitle).height();

        const qreal connectorDiameter = connectorRadius * 2;

        const QRectF imageRect = nRect.adjusted(connectorDiameter, offsetFromTitle,
                                                -connectorDiameter, -3);

        QImage scaledImg = epwdip::image_to_qimage(img);
        scaledImg = scaledImg.scaled(imageRect.width(), imageRect.height(),
                                     Qt::KeepAspectRatio);

        p->drawImage(imageRect, scaledImg);
    }
}

void NodeGui::mousePressEvent(QGraphicsSceneMouseEvent *e)
{
    if (e->modifiers() & Qt::ControlModifier) {
        e->ignore();

        return;
    }

    const QPolygonF markFinalControlRect =
            mapToScene(mapToItem(this, markFinalRect()));

    const QPointF ePos = e->pos();

    if (markFinalControlRect.containsPoint(e->scenePos(),
                                           Qt::OddEvenFill)) {
        if (!m_node->isFinal()) {
            m_node->setFinal();

            scene()->update();

            m_nodeGraph->notifyNodeFinalMarked(this);

            e->ignore();
        }
    }
    else if (inputConnectorBoundingRect().contains(ePos) && m_node->previous()) {
        m_eState = EventState::InitInputConnectorState;
    }
    else if (outputConnectorBoundingRect().contains(ePos)) {
        m_eState = EventState::InitOutputConnectorState;

        m_outputLink = new Link(this);
        m_nodeGraph->scene()->addItem(m_outputLink);
    }
    else {
        m_eState = EventState::MoveState;

        QGraphicsItem::mousePressEvent(e);
    }
}

void NodeGui::mouseMoveEvent(QGraphicsSceneMouseEvent *e)
{
    const QPointF eScenePos = e->scenePos();

    if (m_eState == EventState::InitInputConnectorState) {
        m_eState = EventState::DraggingInputState;
    }
    else if (m_eState == EventState::InitOutputConnectorState) {
        m_eState = EventState::DraggingOutputState;
    }
    else if (m_eState == EventState::DraggingInputState) {
        m_inputLink->setToPos(eScenePos);
        m_nodeGraph->update();
    }
    else if (m_eState == EventState::DraggingOutputState) {
        m_outputLink->setToPos(eScenePos);
        m_nodeGraph->update();
    }
    else if (m_eState == EventState::MoveState) {
        QGraphicsItem::mouseMoveEvent(e);

        if (m_outputLink) {
            m_outputLink->trackNodes();
            m_nodeGraph->update();
        }

        if (m_inputLink) {
            m_inputLink->trackNodes();
            m_nodeGraph->update();
        }
    }
}

void NodeGui::mouseReleaseEvent(QGraphicsSceneMouseEvent *e)
{
    const QPointF eScenePos = e->scenePos();

    if (m_eState == EventState::DraggingOutputState) {
        NodeGui *nodeUnderMouse = m_nodeGraph->nodeAt(eScenePos);

        bool cancelLinking = true;

        if (nodeUnderMouse && nodeUnderMouse != this) {
            const QRectF destConnectorRect
                    = nodeUnderMouse->inputConnectorBRectScene();

            if (destConnectorRect.contains(eScenePos)) {
                m_node->setNext(nodeUnderMouse->node());

                m_outputLink->setInput(nodeUnderMouse);
                nodeUnderMouse->m_inputLink = m_outputLink;

                cancelLinking = false;
            }
        }

        if (cancelLinking) {
            m_nodeGraph->scene()->removeItem(m_outputLink);
            delete m_outputLink;
            m_outputLink = nullptr;
        }
    }
    else if (m_eState == EventState::DraggingInputState) {
        NodeGui *nodeUnderMouse = m_nodeGraph->nodeAt(eScenePos);

//        bool cancelLinking = true;

//        if (nodeUnderMouse) {
//            const QRectF destConnectorRect
//                    = nodeUnderMouse->inputConnectorBRectScene();

//            if (destConnectorRect.contains(eScenePos)) {
//                m_node->setNext(nodeUnderMouse->node());

//                m_outputLink->setInput(nodeUnderMouse);
//                nodeUnderMouse->m_inputLink = m_outputLink;

//                cancelLinking = false;
//            }
//        }

//        if (cancelLinking) {
//            m_nodeGraph->scene()->removeItem(m_outputLink);
//            delete m_outputLink;
//            m_outputLink = nullptr;
//        }
    }
    else if (m_eState == EventState::MoveState) {
        QGraphicsItem::mouseReleaseEvent(e);
    }

    m_eState = EventState::NoEventState;
}

QRectF NodeGui::nodeRect() const
{
    const QFontMetricsF metrics = qApp->fontMetrics();
    const QString nodeTitle = QString::fromStdString(m_node->effectName());

    QRectF ret = metrics.boundingRect(nodeTitle);
    const int padX = 25;
    const int padY = 15;

    ret.adjust(-padX, -padY, padX, padY * 3);
    ret.translate(-ret.center());

    return ret;
}

qreal NodeGui::connectorRadii() const
{
    return 7;
}

QPointF NodeGui::inputConnectorCenter() const
{
    const QRectF nRect = nodeRect();
    const qreal rectCy = nRect.center().y();

    const QPointF ret(nRect.left(), rectCy);

    return ret;
}

QPointF NodeGui::inputConnectorCenterScenePos() const
{
    return mapToScene(inputConnectorCenter());
}

QRectF NodeGui::inputConnectorBoundingRect() const
{
    const QPointF center = inputConnectorCenter();
    const qreal cx = center.x();
    const qreal cy = center.y();

    const qreal side = connectorRadii();
    const qreal diameter = side * 2;

    QRectF ret(cx - side, cy - side, diameter, diameter);

    return ret;
}

QRectF NodeGui::inputConnectorBRectScene() const
{
    QRectF ret = mapRectToScene(inputConnectorBoundingRect());

    return ret;
}

QPointF NodeGui::outputConnectorCenter() const
{
    const QRectF nRect = nodeRect();
    const qreal rectCy = nRect.center().y();

    const QPointF ret(nRect.right(), rectCy);

    return ret;
}

QPointF NodeGui::outputConnectorCenterScenePos() const
{
    return mapToScene(outputConnectorCenter());
}

QRectF NodeGui::outputConnectorBoundingRect() const
{
    const QPointF center = outputConnectorCenter();
    const qreal cx = center.x();
    const qreal cy = center.y();

    const qreal side = connectorRadii();
    const qreal diameter = side * 2;

    QRectF ret(cx - side, cy - side, diameter, diameter);

    return ret;
}

QRectF NodeGui::outputConnectorBRectScene() const
{
    QRectF ret = mapRectToScene(outputConnectorBoundingRect());

    return ret;
}

QRectF NodeGui::markFinalRect() const
{
    const QRectF nRect = nodeRect();

    const qreal size = 10;

    QRectF ret(nRect.right() - size, nRect.top(), size, size + 5);

    return ret;
}
