#include "viewermanager.h"

#include "imagenode.h"
#include "viewer.h"


ViewerManager::ViewerManager(QWidget *parent) :
    QSplitter(Qt::Horizontal, parent),
    m_viewerNodesToViewer()
{}

void ViewerManager::addViewer(ImageNode *viewerNode)
{
    auto viewerFound = m_viewerNodesToViewer.find(viewerNode);

    if (viewerFound != m_viewerNodesToViewer.end()) {
        return;
    }

    auto viewer = new Viewer(this);
    addWidget(viewer);

    m_viewerNodesToViewer.insert({viewerNode, viewer});
}

Viewer *ViewerManager::viewer(ImageNode *viewerNode) const
{
    auto viewerFound = m_viewerNodesToViewer.find(viewerNode);

    if (viewerFound == m_viewerNodesToViewer.end()) {
        return nullptr;
    }

    Viewer *ret = viewerFound->second;

    return ret;
}
