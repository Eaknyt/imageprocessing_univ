#ifndef H_NODEGRAPH_H
#define H_NODEGRAPH_H

#include <memory>

#include <QtWidgets/QGraphicsView>

class ImageNode;
class EffectsFactoriesDB;
class NodeGui;
class AddNodeDialog;

class NodeGraph : public QGraphicsView
{
    Q_OBJECT

public:
    explicit NodeGraph(EffectsFactoriesDB *effectFactories, QWidget *parent = nullptr);
    ~NodeGraph();

    void addNode(std::unique_ptr<ImageNode> &&node);

    void notifyNodeFinalMarked(NodeGui *nodeGui);

    NodeGui *nodeAt(const QPointF &pos) const;

Q_SIGNALS:
    void nodeAdded(NodeGui *nodeGui);
    void nodeMarkedFinal(NodeGui *nodeGui);

protected:
    void drawBackground(QPainter *painter, const QRectF &rect) override;

    void showEvent(QShowEvent *) override;

    void keyPressEvent(QKeyEvent *e) override;

private Q_SLOTS:
    void onNodeItemActivated(const QString &effectFactoryName);

private:
    void openNodesDialog();

    void unselectAllNodes();

    qreal nearestPowerOf10(qreal value);
    void computeGridOffsets();

    void paintGrid(QPainter *p, const QRectF &rect);
    void paintGridLineAndTickX(QPainter *p, qreal y, const QRectF &rect);
    void printGridLineAndTickY(QPainter *p, qreal x, const QRectF &rect);

private:
    EffectsFactoriesDB *m_effectFactoriesDB;
    AddNodeDialog *m_addNodeDialog;

    std::unique_ptr<ImageNode> m_viewerNode;

    NodeGui *m_lastAddedNodeGui;

    QPointF m_gridOffsets;

    bool m_blockForwardWheel;
};

#endif // H_NODEGRAPH_H
