#ifndef H_IMAGENODE_H
#define H_IMAGENODE_H

#include <memory>

#include "epwdip/image.h"

#include "effects/effect.h"

class ImageNode
{
public:
    ImageNode();
    ~ImageNode();

    ImageNode *previous() const;
    void setPrevious(ImageNode *node);

    ImageNode *next() const;
    void setNext(ImageNode *node);

    inline void disconnectInput()
    { setPrevious(nullptr); }

    inline void disconnectOutput()
    { setNext(nullptr); }

    bool isDirty() const;
    void setDirty(bool dirty);

    bool isFinal() const;
    void setFinal();

    Effect *effect() const;
    void setEffect(std::unique_ptr<Effect> &&effect);

    std::string effectName() const;

    const epwdip::Image &evaluate();

private:
    ImageNode *m_previous;
    ImageNode *m_next;

    std::unique_ptr<Effect> m_effect;

    epwdip::Image m_cache;

    bool m_isDirty;
    bool m_isFinal;
};

#endif // H_IMAGENODE_H
