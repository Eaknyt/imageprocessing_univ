#ifndef H_NODEGUI_H
#define H_NODEGUI_H

#include <memory>

#include <QtWidgets/QGraphicsRectItem>

class ImageNode;
class NodeGraph;

class NodeGui : public QGraphicsRectItem
{
public:
    class Link : public QGraphicsPathItem
    {
    public:
        enum Shape
        {
            LineShape,
            CurveShape
        };

        Link(NodeGui *output);

        QPointF fromPos() const;
        void setFromPos(const QPointF &scenePos);

        QPointF toPos() const;
        void setToPos(const QPointF &scenePos);

        NodeGui *output() const;
        void setOutput(NodeGui *node);

        NodeGui *input() const;
        void setInput(NodeGui *node);

        void trackNodes();

    private:
        void computePath();

    private:
        QPointF m_fromPos;
        QPointF m_toPos;

        NodeGui *m_inputNode;
        NodeGui *m_outputNode;

        Shape m_shape;
    };

    enum { Type = UserType + 1 };

    enum class EventState
    {
        NoEventState,
        MoveState,
        InitInputConnectorState,
        InitOutputConnectorState,
        DraggingOutputState,
        DraggingInputState
    };

    NodeGui(std::unique_ptr<ImageNode> &&node, NodeGraph *graph,
            QGraphicsItem *parent = nullptr);
    ~NodeGui();

    int type() const;

    ImageNode *node() const;

    QColor color() const;
    void setColor(const QColor &color);

    Link *outputLink() const;
    Link *inputLink() const;

    QRectF boundingRect() const override;
    QPainterPath shape() const override;

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
               QWidget *widget) override;

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *e) override;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *e) override;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *e) override;

private:
    QRectF nodeRect() const;
    qreal connectorRadii() const;

    QPointF inputConnectorCenter() const;
    QPointF inputConnectorCenterScenePos() const;
    QRectF inputConnectorBoundingRect() const;
    QRectF inputConnectorBRectScene() const;

    QPointF outputConnectorCenter() const;
    QPointF outputConnectorCenterScenePos() const;
    QRectF outputConnectorBoundingRect() const;
    QRectF outputConnectorBRectScene() const;

    QRectF markFinalRect() const;

private:
    std::unique_ptr<ImageNode> m_node;

    NodeGraph *m_nodeGraph;

    QColor m_color;

    EventState m_eState;

    Link *m_outputLink;
    Link *m_inputLink;
};

#endif // H_NODEGUI_H
