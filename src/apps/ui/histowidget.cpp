#include "histowidget.h"

#include <QtCharts/QLineSeries>
#include <QtCharts/QValueAxis>

#include "epwdip/histogram.h"
#include "epwdip/image.h"


using namespace QtCharts;
using namespace epwdip;


HistoWidget::HistoWidget(QWidget *parent) :
    QChartView(parent),
    m_graySeries(new QLineSeries(this)),
    m_redSeries(new QLineSeries(this)),
    m_greenSeries(new QLineSeries(this)),
    m_blueSeries(new QLineSeries(this)),
    m_chart(new QChart)
{
    m_graySeries->setColor(QColor("#888888"));
    m_redSeries->setColor(QColor("#FF0000"));
    m_greenSeries->setColor(QColor("#00FF00"));
    m_blueSeries->setColor(QColor("#0000FF"));

    m_chart->legend()->hide();

    setRenderHint(QPainter::Antialiasing);
    setChart(m_chart);
}

void HistoWidget::setImage(const Image &img)
{
    if (!img.isValid()) {
        return;
    }

    int yMax = 0;

    if (img.format() != Image::Format::Rgb888Format) {
        std::vector<int> hist = histogram(img);

        removeSeries();
        updateSeries(m_graySeries, hist);

        yMax = (*std::max_element(hist.begin(), hist.end()));
    }
    else {
        std::array<std::vector<int>, 3> hists = histogramRgb(img);

        removeSeries();
        updateSeries(m_redSeries, hists[0]);
        updateSeries(m_greenSeries, hists[1]);
        updateSeries(m_blueSeries, hists[2]);

        int redMax = (*std::max_element(hists[0].begin(), hists[0].end()));
        int greenMax = (*std::max_element(hists[1].begin(), hists[1].end()));
        int blueMax = (*std::max_element(hists[2].begin(), hists[2].end()));

        std::vector<int> maxs {redMax, greenMax, blueMax};
        yMax = *(std::max_element(maxs.begin(), maxs.end()));
    }

    m_chart->createDefaultAxes();

    auto xAxis = qobject_cast<QValueAxis *>(m_chart->axisX());
    xAxis->setMax(255);
    xAxis->setLabelFormat("%i");

    auto yAxis = qobject_cast<QValueAxis *>(m_chart->axisY());
    yAxis->setMax(yMax);
    yAxis->setLabelFormat("%i");

}

void HistoWidget::removeSeries()
{
    QList<QAbstractSeries *> currentSeries = m_chart->series();
    int seriesCount = currentSeries.size();

    if (seriesCount > 1) {
        m_chart->removeSeries(m_redSeries);
        m_chart->removeSeries(m_greenSeries);
        m_chart->removeSeries(m_blueSeries);
    }
    else if (seriesCount == 1) {
        m_chart->removeSeries(m_graySeries);
    }
}

void HistoWidget::updateSeries(QLineSeries *series,
                               const std::vector<int> hist)
{
    series->clear();

    for (std::size_t i = 0; i < hist.size(); ++i) {
        series->append(i, hist[i]);
    }

    m_chart->addSeries(series);
}
