#include "effectfactoriesdatabase.h"

#include "effects/channeleffect.h"
#include "effects/closeeffect.h"
#include "effects/emptyeffect.h"
#include "effects/erodeeffect.h"
#include "effects/dilateeffect.h"
#include "effects/fileeffect.h"
#include "effects/filleffect.h"
#include "effects/inpainteffect.h"
#include "effects/openeffect.h"
#include "effects/thresholdeffect.h"
#include "effects/viewereffect.h"

EffectsFactoriesDB::EffectsFactoriesDB() :
    m_effectFactories()
{
    registerEffectFactories();
}

EffectsFactoriesDB::~EffectsFactoriesDB()
{}

std::vector<std::string> EffectsFactoriesDB::factoriesNames() const
{
    std::vector<std::string> ret;
    ret.reserve(m_effectFactories.size());

    for (const auto &p : m_effectFactories) {
        ret.push_back(p.first);
    }

    return ret;
}

AbstractEffectFactory *EffectsFactoriesDB::factory
(const std::string &factoryName) const
{
    AbstractEffectFactory *ret = nullptr;

    auto factoryIt = m_effectFactories.find(factoryName);

    if (factoryIt != m_effectFactories.end()) {
        ret = factoryIt->second.get();
    }

    return ret;
}

void EffectsFactoriesDB::registerEffectFactories()
{
    registerEffectFactory(std::move(std::make_unique<ViewerEffectFactory>()));

    registerEffectFactory(std::move(std::make_unique<ChannelEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<CloseEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<EmptyEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<ErodeEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<DilateEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<FileEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<FillEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<InpaintEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<OpenEffectFactory>()));
    registerEffectFactory(std::move(std::make_unique<ThresholdEffectFactory>()));
}

void EffectsFactoriesDB::registerEffectFactory
(std::unique_ptr<AbstractEffectFactory> &&factory)
{
    m_effectFactories.emplace(factory->name(), std::move(factory));
}
