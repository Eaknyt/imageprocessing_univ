#ifndef H_VIEWERMANAGER_H
#define H_VIEWERMANAGER_H

#include <map>

#include <QtWidgets/QSplitter>


class ImageNode;
class Viewer;

class ViewerManager : public QSplitter
{
    Q_OBJECT

public:
    explicit ViewerManager(QWidget *parent = nullptr);

    void addViewer(ImageNode *viewerNode);
    Viewer *viewer(ImageNode *viewerNode) const;

private:
    std::map<ImageNode *, Viewer *> m_viewerNodesToViewer;
};

#endif // H_VIEWERMANAGER_H
