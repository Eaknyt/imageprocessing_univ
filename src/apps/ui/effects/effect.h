#ifndef H_EFFECT_H
#define H_EFFECT_H

#include <memory>
#include <string>

#include <QVariantMap>

namespace epwdip {
class Image;
} //namespace epwdip

class AbstractEffectFactory;
class ImageNode;

class Effect
{
public:
    explicit Effect(ImageNode *node, const AbstractEffectFactory *factory);
    ~Effect();

    std::string name() const;

    ImageNode *node() const;

    QVariant property(const std::string &name) const;
    void setProperty(const std::string &name, const QVariant &value);

    QVariantMap properties() const;

    void apply(epwdip::Image &img);

protected:
    ImageNode *m_node;

private:
    const AbstractEffectFactory *m_factory;

    std::string m_name;
    QVariantMap m_properties;
};

#endif // H_EFFECT_H
