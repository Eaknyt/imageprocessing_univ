#ifndef H_CLOSEEFFECT_H
#define H_CLOSEEFFECT_H

#include "effectfactory.h"

class Effect;

class CloseEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_CLOSEEFFECT_H
