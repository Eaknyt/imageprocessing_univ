#ifndef H_CHANNELEFFECT_H
#define H_CHANNELEFFECT_H

#include "effectfactory.h"

class ChannelEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_CHANNELEFFECT_H
