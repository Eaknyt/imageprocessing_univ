#ifndef H_ERODEEFFECT_H
#define H_ERODEEFFECT_H

#include "effectfactory.h"

class ErodeEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};


#endif // H_ERODEEFFECT_H
