#include "emptyeffect.h"

#include "../imagenode.h"

std::string EmptyEffectFactory::name() const
{
    return "Empty";
}

void EmptyEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("Width", 0);
    effect->setProperty("Height", 0);
}

void EmptyEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    const int width = effect->property("Width").toInt();
    const int height = effect->property("Height").toInt();

    img = epwdip::Image(width, height, epwdip::Image::Format::Rgb888Format);
}
