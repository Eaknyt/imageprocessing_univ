#ifndef H_DILATEEFFECT_H
#define H_DILATEEFFECT_H

#include "effectfactory.h"

class DilateEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_DILATEEFFECT_H
