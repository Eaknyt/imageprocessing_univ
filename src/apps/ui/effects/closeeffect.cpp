#include "closeeffect.h"

#include "epwdip/morpho.h"

#include "effect.h"

std::string CloseEffectFactory::name() const
{
    return "Close";
}

void CloseEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("iterations", 1);
}

void CloseEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    int iterations = effect->property("iterations").toInt();

    for (int i = 0; i < iterations; ++i) {
        img = epwdip::closeImage(img);
    }
}
