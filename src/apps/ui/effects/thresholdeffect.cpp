#include "thresholdeffect.h"

#include "epwdip/thresholds.h"

#include "../imagenode.h"

std::string ThresholdEffectFactory::name() const
{
    return "Threshold";
}

void ThresholdEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("Level_1", 0);
    effect->setProperty("Level_2", 0);
    effect->setProperty("Level_3", 0);
}

void ThresholdEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    int t1 = effect->property("Level_1").toInt();
    int t2 = effect->property("Level_2").toInt();
    int t3 = effect->property("Level_3").toInt();

    if (t3 > 0) {
        img = epwdip::thresholded(img, t1, t2, t3);
    }
    else if (t2 > 0) {
        img = epwdip::thresholded(img, t1, t2);
    }
    else {
        img = epwdip::thresholded(img, t1);
    }
}
