#include "effectfactory.h"

#include "effect.h"

AbstractEffectFactory::AbstractEffectFactory()
{}

AbstractEffectFactory::~AbstractEffectFactory()
{}

void AbstractEffectFactory::createEffectProperties(Effect *) const
{}

std::unique_ptr<Effect> AbstractEffectFactory::createEffect(ImageNode *node) const
{
    auto ret = std::make_unique<Effect>(node, this);

    createEffectProperties(ret.get());

    return ret;
}
