#ifndef H_VIEWEREFFECT_H
#define H_VIEWEREFFECT_H

#include "effectfactory.h"

class ViewerEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_VIEWEREFFECT_H
