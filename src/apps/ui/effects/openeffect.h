#ifndef H_OPENEFFECT_H
#define H_OPENEFFECT_H

#include "effectfactory.h"

class OpenEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_OPENEFFECT_H
