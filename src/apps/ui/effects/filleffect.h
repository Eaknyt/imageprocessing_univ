#ifndef H_FILLEFFECT_H
#define H_FILLEFFECT_H

#include "effectfactory.h"

class FillEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_FILLEFFECT_H
