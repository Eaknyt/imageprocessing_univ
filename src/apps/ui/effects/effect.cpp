#include "effect.h"

#include "epwdip/image.h"

#include "../imagenode.h"

#include "effectfactory.h"

Effect::Effect(ImageNode *node, const AbstractEffectFactory *factory) :
    m_node(node),
    m_factory(factory),
    m_name(factory->name()),
    m_properties()
{
    Q_ASSERT (factory);
}

Effect::~Effect()
{}

std::string Effect::name() const
{
    return m_factory->name();
}

ImageNode *Effect::node() const
{
    return m_node;
}

QVariant Effect::property(const std::string &name) const
{
    const QString name_q = QString::fromStdString(name);

    QVariant ret;

    if (m_properties.contains(name_q)) {
        ret = m_properties[name_q];
    }

    return ret;
}

void Effect::setProperty(const std::string &name, const QVariant &value)
{
    QVariant &oldVal = m_properties[QString::fromStdString(name)];

    if (oldVal != value) {
        oldVal = value;

        m_node->setDirty(true);
    }
}

QVariantMap Effect::properties() const
{
    return m_properties;
}

void Effect::apply(epwdip::Image &img)
{
    m_factory->apply(this, img);
}
