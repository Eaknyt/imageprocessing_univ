#include "dilateeffect.h"

#include "epwdip/morpho.h"

#include "effect.h"

std::string DilateEffectFactory::name() const
{
    return "Dilate";
}

void DilateEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("iterations", 1);
}

void DilateEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    int iterations = effect->property("iterations").toInt();

    for (int i = 0; i < iterations; ++i) {
        img = epwdip::dilate(img);
    }
}
