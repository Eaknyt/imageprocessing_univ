#ifndef H_EMPTYEFFECT_H
#define H_EMPTYEFFECT_H

#include "effectfactory.h"

class EmptyEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};


#endif // H_EMPTYEFFECT_H
