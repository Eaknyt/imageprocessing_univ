#include "fileeffect.h"

#include <QtCore/QUrl>

#include "epwdip/image.h"

#include "effect.h"

std::string FileEffectFactory::name() const
{
    return "File";
}

void FileEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("filePath", QUrl());
}

void FileEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    QString filePath = effect->property("filePath").toUrl().toString();
    filePath.remove("file://");

    img = epwdip::Image(filePath.toStdString());
}
