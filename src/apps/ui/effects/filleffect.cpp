#include "filleffect.h"

#include <QtGui/QColor>

#include "epwdip/color.h"

#include "../imagenode.h"

std::string FillEffectFactory::name() const
{
    return "Fill";
}

void FillEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("Color", QColor(Qt::black));
}

void FillEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    const QColor color = effect->property("Color").value<QColor>();
    const epwdip::Color rawColor(color.redF(), color.greenF(), color.blueF(),
                                 color.alphaF());

    img = epwdip::Image(img.data(), img.width(), img.height(),
                        epwdip::Image::Format::Rgb888Format);

    img.clear(rawColor.red(), rawColor.green(), rawColor.blue());
}
