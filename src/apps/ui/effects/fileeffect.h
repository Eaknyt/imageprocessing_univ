#ifndef H_FILEEFFECT_H
#define H_FILEEFFECT_H

#include "effectfactory.h"

class FileEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_FILEEFFECT_H
