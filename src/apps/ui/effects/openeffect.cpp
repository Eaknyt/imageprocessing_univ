#include "openeffect.h"

#include "epwdip/morpho.h"

#include "effect.h"

std::string OpenEffectFactory::name() const
{
    return "Open";
}

void OpenEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("iterations", 1);
}

void OpenEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    int iterations = effect->property("iterations").toInt();

    for (int i = 0; i < iterations; ++i) {
        img = epwdip::closeImage(img);
    }
}
