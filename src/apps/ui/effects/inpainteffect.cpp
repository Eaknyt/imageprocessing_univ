#include "inpainteffect.h"

#include <QtCore/QUrl>

#include <QtGui/QColor>

#include "epwdip/inpaint.h"

#include "../imagenode.h"
#include "../specialmetatypes.h"

std::string InpaintEffectFactory::name() const
{
    return "Inpaint";
}

void InpaintEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("Iterations", 1);
    effect->setProperty("Mask color", QColor(Qt::white));
    effect->setProperty("Mask", QUrl());

    EnumPropertyType method;
    method.choices << "Kernel Based" << "Sobel and gradient Based"
                   << "Anisotropic Diffusion";

    effect->setProperty("Method", QVariant::fromValue(method));
}

void InpaintEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    const int iterations = effect->property("Iterations").toInt();
    const QColor maskColor = effect->property("Mask color").value<QColor>();
    QString filePath = effect->property("Mask").toUrl().toString();
    filePath.remove("file://");

    const EnumPropertyType method = effect->property("Method").value<EnumPropertyType>();

    const epwdip::Color rawMaskColor(maskColor.redF(), maskColor.greenF(),
                                     maskColor.blueF());

    const epwdip::Image mask(filePath.toStdString());

    if (method.currentIndex == 0) {
        img = epwdip::inpaint_kernel(img, mask, rawMaskColor, iterations);
    }
    else if (method.currentIndex == 1) {
        img = epwdip::inpaint_convolution(img, mask, rawMaskColor, iterations);
    }
    else if (method.currentIndex == 2) {
        img = epwdip::inpaint_anisotropic(img, mask, rawMaskColor, iterations);
    }
}
