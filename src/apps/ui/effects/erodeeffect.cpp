#include "erodeeffect.h"

#include "epwdip/morpho.h"

#include "effect.h"

std::string ErodeEffectFactory::name() const
{
    return "Erode";
}

void ErodeEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("iterations", 1);
}

void ErodeEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    int iterations = effect->property("iterations").toInt();

    for (int i = 0; i < iterations; ++i) {
        img = epwdip::erode(img);
    }
}
