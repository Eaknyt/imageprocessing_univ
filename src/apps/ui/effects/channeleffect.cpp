#include "channeleffect.h"

#include "epwdip/channels.h"

#include "effect.h"

std::string ChannelEffectFactory::name() const
{
    return "Channel";
}

void ChannelEffectFactory::createEffectProperties(Effect *effect) const
{
    effect->setProperty("red", true);
    effect->setProperty("green", true);
    effect->setProperty("blue", true);
}

void ChannelEffectFactory::apply(Effect *effect, epwdip::Image &img) const
{
    bool redEnabled = effect->property("red").toBool();
    bool greenEnabled = effect->property("green").toBool();
    bool blueEnabled = effect->property("blue").toBool();

    if (!redEnabled && !greenEnabled && !blueEnabled) {
        img.clear(0);
    }
    else if (redEnabled && !greenEnabled && !blueEnabled) {
        img = epwdip::channel(img, epwdip::Channel::RedChannel);
    }
    else if (!redEnabled && greenEnabled && !blueEnabled) {
        img = epwdip::channel(img, epwdip::Channel::GreenChannel);
    }
    else if (!redEnabled && !greenEnabled && blueEnabled) {
        img = epwdip::channel(img, epwdip::Channel::BlueChannel);
    }
    else if (redEnabled && greenEnabled && !blueEnabled) {
        img = epwdip::channels(img,
                               epwdip::Channel::RedChannel,
                               epwdip::Channel::GreenChannel);
    }
    else if (redEnabled && !greenEnabled && blueEnabled) {
        img = epwdip::channels(img,
                               epwdip::Channel::RedChannel,
                               epwdip::Channel::BlueChannel);
    }
    else if (!redEnabled && greenEnabled && blueEnabled) {
        img = epwdip::channels(img,
                               epwdip::Channel::GreenChannel,
                               epwdip::Channel::BlueChannel);
    }
}
