#ifndef H_EFFECTFACTORY_H
#define H_EFFECTFACTORY_H

#include <memory>
#include <string>

namespace epwdip {
class Image;
} //namespace epwdip

class Effect;
class ImageNode;

class AbstractEffectFactory
{
public:
    AbstractEffectFactory();
    ~AbstractEffectFactory();

    virtual std::string name() const = 0;

    virtual void createEffectProperties(Effect *effect) const;

    std::unique_ptr<Effect> createEffect(ImageNode *node) const;

    virtual void apply(Effect *effect, epwdip::Image &img) const = 0;
};

#endif // H_EFFECTFACTORY_H
