#ifndef H_THRESHOLDEFFECT_H
#define H_THRESHOLDEFFECT_H

#include "effectfactory.h"

class ThresholdEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_THRESHOLDEFFECT_H
