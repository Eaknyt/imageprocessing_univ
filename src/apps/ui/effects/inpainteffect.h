#ifndef H_INPAINTEFFECT_H
#define H_INPAINTEFFECT_H

#include "effectfactory.h"

class InpaintEffectFactory : public AbstractEffectFactory
{
public:
    std::string name() const override;
    void createEffectProperties(Effect *effect) const override;
    void apply(Effect *effect, epwdip::Image &img) const override;
};

#endif // H_INPAINTEFFECT_H
