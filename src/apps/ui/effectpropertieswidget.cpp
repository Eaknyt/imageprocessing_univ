#include "effectpropertieswidget.h"

#include <QtCore/QDebug>
#include <QtCore/QMetaProperty>

#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QSpinBox>

#include "imagenode.h"
#include "nodegui.h"
#include "specialmetatypes.h"

#include "effects/effect.h"

#include "widgets/advslider.h"
#include "widgets/coloredit.h"
#include "widgets/urledit.h"


////////////////////// EffectPropertyEditorDelegate //////////////////////

class EffectPropertyEditorDelegate
{
public:
    EffectPropertyEditorDelegate(EffectPropertiesWidget *parent);

    QWidget *createEditor(const QString &propName, const QVariant &value,
                          QWidget *parent) const;

private:
    EffectPropertiesWidget *m_parent;
};

EffectPropertyEditorDelegate::EffectPropertyEditorDelegate
(EffectPropertiesWidget *parent) :
    m_parent(parent)
{}

QWidget *EffectPropertyEditorDelegate::createEditor
(const QString &propName, const QVariant &value, QWidget *parent) const
{
    QWidget *ret = nullptr;

    int propType = value.userType();

    QString editorProperty;

    // Ugly trick for supporting multiple editors per variant type
    if (propName.contains("Level_")) {
        editorProperty = "value";

        ret = new AdvSlider(parent);

        QObject::connect(qobject_cast<AdvSlider *>(ret), &AdvSlider::valueChanged,
                         [=] (int val) {
            m_parent->updateEffect(propName, val);
        });
    }
    else if (propType == QMetaType::Int) {
        editorProperty = "value";

        ret = new QSpinBox(parent);
        ret->setProperty("maximum", 4096);

        QObject::connect(qobject_cast<QSpinBox *>(ret), static_cast<void (QSpinBox::*) (int)>(&QSpinBox::valueChanged),
                         [=] (int val) {
            m_parent->updateEffect(propName, val);
        });
    }
    else if (propType == QMetaType::Double) {
        editorProperty = "value";

        ret = new QDoubleSpinBox(parent);

        QObject::connect(qobject_cast<QDoubleSpinBox *>(ret), static_cast<void (QDoubleSpinBox::*) (double)>(&QDoubleSpinBox::valueChanged),
                         [=] (double val) {
            m_parent->updateEffect(propName, val);
        });
    }
    else if (propType == QMetaType::Bool) {
        editorProperty = "checked";

        ret = new QCheckBox(parent);

        QObject::connect(qobject_cast<QCheckBox *>(ret), &QCheckBox::toggled,
                         [=] (bool val) {
            m_parent->updateEffect(propName, val);
        });
    }
    else if (propType == QMetaType::QUrl) {
        editorProperty = "url";

        ret = new UrlEdit(parent);

        const auto nameFilters = QStringList() << "Supported image files (*.pgm *.ppm)";
        ret->setProperty("nameFilters", nameFilters);

        QObject::connect(qobject_cast<UrlEdit *>(ret), &UrlEdit::urlChanged,
                         [=] (const QUrl &val) {
            m_parent->updateEffect(propName, val);
        });
    }
    else if (propType == QMetaType::QColor) {
        editorProperty = "color";

        ret = new ColorEdit(parent);

        QObject::connect(qobject_cast<ColorEdit *>(ret), &ColorEdit::colorChanged,
                         [=] (const QColor val) {
            m_parent->updateEffect(propName, val);
        });
    }
    else if (propType == enumPropertyMetaType()) {
        editorProperty = "currentIndex";

        const QStringList choices = value.value<EnumPropertyType>().choices;

        ret = new QComboBox(parent);

        auto typedEditor = qobject_cast<QComboBox *>(ret);
        typedEditor->setCurrentIndex(value.value<EnumPropertyType>().currentIndex);
        typedEditor->addItems(choices);

        QObject::connect(typedEditor, static_cast<void (QComboBox::*) (int)>(&QComboBox::currentIndexChanged),
                         [=] (int index) {
            EnumPropertyType newVal(index);
            newVal.choices = choices;

            m_parent->updateEffect(propName, QVariant::fromValue(newVal));
        });
    }

    if (!ret) {
        qCritical() << "No editor associated with" << value.typeName();
    }
    else {
        ret->setProperty(editorProperty.toStdString().c_str(), value);
    }

    return ret;
}


////////////////////// EffectPropertiesWidget //////////////////////

EffectPropertiesWidget::EffectPropertiesWidget(QWidget *parent) :
    QWidget(parent),
    m_leftSideFrame(new QFrame(this)),
    m_centralLayout(new QVBoxLayout(m_leftSideFrame)),
    m_nodeLabel(new QLabel(this)),
    m_innerPropsWidget(new QWidget(this)),
    m_propsFormLayout(nullptr),
    m_editorDelegate(std::make_unique<EffectPropertyEditorDelegate>(this)),
    m_currentNodeGui(nullptr)
{
    m_leftSideFrame->setAutoFillBackground(true);
    m_leftSideFrame->setFixedWidth(3);

    m_nodeLabel->setText("");

    m_centralLayout = new QVBoxLayout;

    m_centralLayout->addWidget(m_nodeLabel);
    m_centralLayout->addSpacing(30);
    m_centralLayout->addWidget(m_innerPropsWidget);
    m_centralLayout->addStretch();

    auto mainLayout = new QHBoxLayout(this);

    mainLayout->addWidget(m_leftSideFrame);
    mainLayout->addLayout(m_centralLayout);
}

EffectPropertiesWidget::~EffectPropertiesWidget()
{}

void EffectPropertiesWidget::setNodeGui(NodeGui *nodeGui)
{
    if (m_currentNodeGui == nodeGui) {
        return;
    }

    m_currentNodeGui = nodeGui;

    if (nodeGui) {
        // Set the color of the left side frame
        QPalette pal = palette();
        pal.setColor(QPalette::Background, nodeGui->color());

        m_leftSideFrame->setPalette(pal);

        // Retrieve node properties
        populatePropsForm();
    }

    setVisible((nodeGui));
}

void EffectPropertiesWidget::updateEffect(const QString &propName,
                                          const QVariant &value)
{
    ImageNode *node = m_currentNodeGui->node();
    Effect *effect = node->effect();

    effect->setProperty(propName.toStdString(), value);

    Q_EMIT effectPropertyEdited(node);

    m_currentNodeGui->update();
}

void EffectPropertiesWidget::populatePropsForm()
{
    const ImageNode *node = m_currentNodeGui->node();

    // Update the label showing the effect name
    const QString effectNameStr = QString::fromStdString(node->effectName());
    m_nodeLabel->setText("Effect : " + effectNameStr);

    // Ugly trick from the hell to clear the form layout
    // (there's a bug fixed only in Qt 5.8 that prevents a QFormLayout to be
    // cleared)
    QLayoutItem *it = m_centralLayout->takeAt(1);
    delete it->widget();
    delete it;

    m_innerPropsWidget = nullptr;
    m_innerPropsWidget = new QWidget(this);
    m_propsFormLayout = new QFormLayout(m_innerPropsWidget);
    m_propsFormLayout->setContentsMargins(0, 0, 0, 0);

    m_centralLayout->insertWidget(1, m_innerPropsWidget);

    // Create editors for effect properties
    const Effect *effect = node->effect();
    const QVariantMap props = effect->properties();

    for (const QString &propName : props.keys()) {
        const QVariant propValue = props.value(propName);

        QWidget *editor = m_editorDelegate->createEditor(propName, propValue,
                                                         m_innerPropsWidget);
        m_propsFormLayout->addRow(propName, editor);
    }
}
