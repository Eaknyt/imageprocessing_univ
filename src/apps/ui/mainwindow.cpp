#include "mainwindow.h"

#include <QtCore/QDebug>
#include <QtCore/QTemporaryFile>

#include <QtGui/QImage>

#include <QtWidgets/QAction>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QFileDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QVBoxLayout>

#include "effectfactoriesdatabase.h"
#include "effectpropertieswidget.h"
#include "imagenode.h"
#include "nodegraph.h"
#include "nodegui.h"
#include "viewer.h"
#include "viewermanager.h"

#include "widgets/advslider.h"


using namespace epwdip;


////////////////////// MainWindow //////////////////////

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_effectsFactoriesDB(std::make_unique<EffectsFactoriesDB>()),
    m_viewerManager(new ViewerManager(this)),
    m_nodeGraph(new NodeGraph(m_effectsFactoriesDB.get(), this))
{
    // Menu bar
    auto myMenuBar = new QMenuBar(this);
    setMenuBar(myMenuBar);

    // Central part
    auto splitter = new QSplitter(this);
    splitter->setHandleWidth(5);

    auto rightSplitter = new QSplitter(Qt::Vertical, splitter);
    rightSplitter->setHandleWidth(5);

    // Node properties panel
    auto nodePropsPanel = new QTabWidget(splitter);
    m_nodePropsWidget = new EffectPropertiesWidget(nodePropsPanel);

    connect(m_nodeGraph->scene(), &QGraphicsScene::selectionChanged,
            this, &MainWindow::onNodeSelectionChanged);

    connect(m_nodeGraph, &NodeGraph::nodeAdded,
            this, &MainWindow::onNodeAdded);

    nodePropsPanel->addTab(m_nodePropsWidget, "Node properties");
    m_nodePropsWidget->setVisible(false);

    //  Compose panels
    rightSplitter->addWidget(nodePropsPanel);

    auto leftSplitter = new QSplitter(Qt::Vertical, splitter);
    leftSplitter->addWidget(m_viewerManager);
    leftSplitter->addWidget(m_nodeGraph);

    splitter->addWidget(leftSplitter);
    splitter->addWidget(rightSplitter);

    setCentralWidget(splitter);

    // S/S connections
//    connect(m_nodeGraph, &NodeGraph::nodeMarkedFinal,
//            this, &MainWindow::onNodeMarkedFinal);

    connect(m_nodePropsWidget, &EffectPropertiesWidget::effectPropertyEdited,
            this, &MainWindow::onEffectPropertyEdited);
}
// MainWindow::MainWindow()

MainWindow::~MainWindow()
{}

void MainWindow::onNodeAdded(NodeGui *nodeGui)
{
    ImageNode *node = nodeGui->node();

    if (node->effectName() != "Viewer") {
        return;
    }

    m_viewerManager->addViewer(node);
}

void MainWindow::onNodeSelectionChanged()
{
    const QGraphicsScene *nodeGraphScene = m_nodeGraph->scene();

    const QList<QGraphicsItem *> selectedItems =
            nodeGraphScene->selectedItems();

    NodeGui *selectedNodeGui = nullptr;

    if (!selectedItems.empty()) {
        selectedNodeGui = qgraphicsitem_cast<NodeGui *>(selectedItems[0]);
    }

    m_nodePropsWidget->setNodeGui(selectedNodeGui);
}

//void MainWindow::onNodeMarkedFinal(NodeGui *nodeGui)
//{
//    ImageNode *node = nodeGui->node();

//    render(node);

//    nodeGui->update();
//}

void MainWindow::onEffectPropertyEdited(ImageNode *node)
{
    ImageNode *viewerNode = findViewerNode(node);

    if (!viewerNode) {
        return;
    }

    render(viewerNode);
}

//ImageNode *MainWindow::findFinalNodeForPath(ImageNode *node) const
//{
//    if (node->isFinal()) {
//        return node;
//    }

//    ImageNode *ret = nullptr;

//    // Search backward
//    ImageNode *prev = node->previous();

//    while (prev) {
//        if (prev->isFinal()) {
//            ret = prev;
//            break;
//        }

//        prev = prev->previous();
//    }

//    // Search forward
//    ImageNode *next = node->next();

//    while (next) {
//        if (next->isFinal()) {
//            ret = next;
//            break;
//        }

//        next = next->next();
//    }

//    return ret;
//}

ImageNode *MainWindow::findViewerNode(ImageNode *node) const
{
    if (node->effectName() == "Viewer") {
        return node;
    }

    ImageNode *ret = nullptr;

    // Search forward
    ImageNode *next = node->next();

    while (next) {
        if (next->effectName() == "Viewer") {
            ret = next;
            break;
        }

        next = next->next();
    }

    return ret;
}

void MainWindow::render(ImageNode *viewerNode)
{
    const Image &toDisplay = viewerNode->evaluate();

    m_viewerManager->viewer(viewerNode)->setImage(toDisplay);
}
