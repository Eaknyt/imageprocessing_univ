#ifndef H_VIEWER_H
#define H_VIEWER_H

#include <QtWidgets/QWidget>

#include "epwdip/image.h"


class Viewer : public QWidget
{
    Q_OBJECT

public:
    explicit Viewer(QWidget *parent = nullptr);

    QString name() const;
    void setName(const QString &name);

    void setImage(const epwdip::Image &img);

protected:
    void resizeEvent(QResizeEvent *e) override;

    void paintEvent(QPaintEvent *e) override;

private:
    QString m_name;

    epwdip::Image m_img;
    QImage m_drawnImg;
};

#endif // H_VIEWER_H
