#ifndef SPECIALMETATYPES_H
#define SPECIALMETATYPES_H

#include <QMetaType>

struct EnumPropertyType
{
    EnumPropertyType() :
        currentIndex(0),
        choices()
    {}

    EnumPropertyType (int i) :
        currentIndex(i),
        choices()
    {}

    int currentIndex;
    QStringList choices;
};

Q_DECLARE_METATYPE(EnumPropertyType)

inline int enumPropertyMetaType()
{
    return qMetaTypeId<EnumPropertyType>();
}

#endif // SPECIALMETATYPES_H
