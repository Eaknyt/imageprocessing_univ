#include "viewer.h"

#include <QtGui/QPaintEvent>
#include <QtGui/QPainter>

#include "epwdiptoqt/helpers.h"


Viewer::Viewer(QWidget *parent) :
    QWidget(parent),
    m_name(),
    m_img(),
    m_drawnImg()
{}

QString Viewer::name() const
{
    return m_name;
}

void Viewer::setName(const QString &name)
{
    if (m_name != name) {
        m_name = name;
    }
}

void Viewer::setImage(const epwdip::Image &img)
{
    QImage qtImg = epwdip::image_to_qimage(img);

    if (m_drawnImg != qtImg) {
        m_drawnImg = qtImg.copy().scaledToHeight(size().height());

        m_img.copy(img);
    }

    update();
}

void Viewer::resizeEvent(QResizeEvent *e)
{
    if (e->size().isNull() || m_img.isNull()) {
        return;
    }

    QImage qtImg = epwdip::image_to_qimage(m_img);

    m_drawnImg = qtImg.scaledToHeight(e->size().height());
}

void Viewer::paintEvent(QPaintEvent *e)
{
    if (m_drawnImg.isNull()) {
        return;
    }

    QPainter p(this);

    p.drawImage(QRect(0, 0, m_drawnImg.width(), m_drawnImg.height()),
                m_drawnImg);
}
