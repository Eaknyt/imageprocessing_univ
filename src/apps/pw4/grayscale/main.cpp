#include <iostream>

#include "epwdip/convert.h"
#include "epwdip/image.h"

using namespace epwdip;

int main(int argc, char **argv)
{
    if (argc != 3) {
        std::cout << "Usage : <imgInPath> <imgOutPath>" << std::endl;
        return 1;
    }

    const std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    if (inputImg.format() != Image::Format::Rgb888Format) {
        std::cerr << "The image must be rgb." << std::endl;
        return 1;
    }

    const std::string imgOutPath(argv[2]);

    Image outputImg = grayscale(inputImg);
    outputImg.write(imgOutPath);

    return 0;
}
