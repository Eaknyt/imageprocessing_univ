#include <iostream>

#include "epwdip/blur.h"
#include "epwdip/image.h"

using namespace epwdip;

int main(int argc, char **argv)
{
    if (argc != 3) {
        std::cout << "Usage : <imgInPath> <imgOutPath>" << std::endl;
        return 1;
    }

    const std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    if (inputImg.format() != Image::Format::Rgb888Format) {
        std::cerr << "The image must be rgb." << std::endl;
        return 1;
    }

    const std::string imgOutPath(argv[2]);

    Image blurredImg = blur(inputImg);
    blurredImg.write(imgOutPath);

    // Blur background
    Image binaryImg("res/processed/fedou_140.pgm");

    Image bkBlurredImg = blur(inputImg, binaryImg);
    bkBlurredImg.write("res/processed/fedou_bk_blurred.pgm");

    //
    Image binaryRef2("res/processed/fedou_eroded_dilated.pgm");

    Image bkBlurredImg2 = blur(inputImg, binaryRef2);
    bkBlurredImg2.write("res/processed/fedou_bk_blurred_final.pgm");

    return 0;
}
