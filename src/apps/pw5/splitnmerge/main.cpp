#include <array>
#include <assert.h>
#include <cmath>
#include <iostream>

#include "epwdip/convert.h"
#include "epwdip/image.h"
#include "epwdip/imagequadtree.h"
#include "epwdip/stat.h"

using namespace epwdip;


void createFinalImg(Quadtree *quadtree, Image &finalImg)
{
    const std::vector<Quadtree *> children = quadtree->children();

    for (int i = 0; i < children.size(); ++i) {
        Quadtree *child = children[i];

        const Image subImg = child->image();

        int gAvg = grayscaleAvg(subImg);

        const int cx = child->x();
        const int cy = child->y();

        for (int y = cy; y < cy + subImg.height(); ++y) {
            for (int x = cx; x < cx + subImg.width(); ++x) {
                finalImg(x, y) = gAvg;
            }
        }

        createFinalImg(child, finalImg);
    }
}

////////////////////// Main //////////////////////

int main(int argc, char **argv)
{
    if (argc != 5) {
        std::cout << "Usage : <imgInPath> <avgOutPath> <finalOutPath> <threshold>" << std::endl;
        return 1;
    }

    const std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    const int w = inputImg.width();
    const int h = inputImg.height();

    if (w % 2 || h % 2) {
        std::cerr << "Input image's dimensions must be power of 2." << std::endl;
        return 1;
    }

    if (inputImg.format() == Image::Format::Rgb888Format) {
        inputImg = grayscale(inputImg);

        inputImg.write("res/processed/img_g.pgm");
    }

    // Write avg image
    const std::string imgAvgPath(argv[2]);

    Image outputAvgImg = grayscaleAvgImage(inputImg);
    outputAvgImg.write(imgAvgPath);

    // Split the input image using a quadtree and a the given threshold
    int threshold = std::stoi(argv[4]);

    Quadtree quadtree(inputImg, 0, 0, nullptr);

    quadtree.split([=] (Quadtree *q) {
        return grayscaleVariance(q->image()) > threshold;
    });

    // Write final image
    const std::string imgFinalPath(argv[3]);

    Image outputFinalImg(w, h, Image::Format::Grayscale8Format);
    createFinalImg(&quadtree, outputFinalImg);

    outputFinalImg.write(imgFinalPath);

    return 0;
}
