#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>

#include "eplot/plot.h"

#include "epwdip/image.h"
#include "epwdip/rangeprofiler.h"


using namespace eplot;
using namespace epwdip;


bool is_number(const std::string &str)
{
    for (const char &c : str) {
        if (!isdigit(c)) {
            return false;
        }
    }

    return true;
}


// Range Profiler
int main(int argc, char **argv)
{
    if (argc != 4) {
        std::cout << "Usage : <imgPath> <r|c> <number> " << std::endl;
        return 1;
    }

    std::string imgPath = argv[1];

    Image inputImg;
    if (!inputImg.read(imgPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    std::string rangeType = argv[2];
    if (rangeType != "r" && rangeType != "c") {
        std::cerr << "Invalid range type." << std::endl;
        return 1;
    }

    try {
        std::string rangeNumber_str = argv[3];
        if (!is_number(rangeNumber_str)) {
            throw std::invalid_argument("Invalid range number");
        }

        int rangeNumber = std::stoi(rangeNumber_str);

        std::vector<int> infos;

        if (rangeType == "r") {
            if (rangeNumber > inputImg.height()) {
                throw std::invalid_argument("Invalid range number");
            }

            infos = profileRow(inputImg, rangeNumber);
        }
        else if (rangeType == "c") {
            if (rangeNumber > inputImg.width()) {
                throw std::invalid_argument("Invalid range number");
            }
            infos = profileCol(inputImg, rangeNumber);
        }

        // Plot
        std::vector<double> points(infos.size());
        std::vector<double> infosd(infos.size());

        for (std::size_t i = 0; i < infos.size(); ++i) {
            points[i] = i;
            infosd[i] = infos[i];
//            std::cout << i << " " << infosd[i] << std::endl;
        }

        plotDirectory("res/processed/");
        plot(points, infosd, "#888888");

        std::string xLabel = (rangeType) == "r" ? "Image width"
                                                : "Image height";

        plotLabels(xLabel, "Grayscale value");

        std::string imgFullName = imgPath.substr(imgPath.find_last_of('/') + 1);
        std::string imgName = imgFullName.substr(0, imgFullName.find_last_of('.'));

        std::string outputFilename = imgName + "_" + rangeType +
                "_" + std::to_string(rangeNumber);

        plotWrite(outputFilename);
    }
    catch (const std::invalid_argument &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}
