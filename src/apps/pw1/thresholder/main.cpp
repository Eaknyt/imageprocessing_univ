#include <iostream>
#include <string>

#include "epwdip/thresholds.h"

using namespace epwdip;

int main(int argc, char **argv)
{
    if (!(3 <= argc && argc <= 5)) {
        std::cout << "Usage : <imgPath> <t1> [t2] [t3]" << std::endl;
        return 1;
    }

    std::string imgPath = argv[1];

    Image inputImg;

    if (!inputImg.read(imgPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    Image outputImg;

    std::string outputFileSuffix = "_";

    // Apply threshold
    int t1 = std::stoi(argv[2]);

    if (argc > 3) {
        int t2 = std::stoi(argv[3]);

        if (argc == 5) {
            int t3 = std::stoi(argv[4]);

            outputFileSuffix += std::to_string(t1) + "_" + std::to_string(t2) +
                    "_" + std::to_string(t3);

            outputImg = thresholded(inputImg, t1, t2, t3);
        }
        else {
            outputFileSuffix += std::to_string(t1) + "_" + std::to_string(t2);

            outputImg = thresholded(inputImg, t1, t2);
        }
    }
    else {
        outputFileSuffix += std::to_string(t1);

        outputImg = thresholded(inputImg, t1);
    }

    // Write output file
    std::string imgFullName = imgPath.substr(imgPath.find_last_of('/') + 1);
    std::string imgName = imgFullName.substr(0, imgFullName.find_last_of('.'));

    std::string ext = (inputImg.format() == Image::Format::Rgb888Format)
            ? ".ppm"
            : ".pgm";

    outputImg.write("res/processed/" + imgName + outputFileSuffix + ext);

    return 0;
}
