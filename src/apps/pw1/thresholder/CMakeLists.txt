project(thresholder)

set(SOURCES
    main.cpp)

# Rules
add_executable(thresholder ${SOURCES})
    
target_link_libraries(thresholder epwdip)

