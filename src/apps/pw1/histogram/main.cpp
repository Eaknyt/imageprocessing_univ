#include <algorithm>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

#include "eplot/plot.h"

#include "epwdip/histogram.h"
#include "epwdip/image.h"

using namespace eplot;
using namespace epwdip;


// Histogram Grayscale
int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cout << "Usage : <imgPath>" << std::endl;
        return 1;
    }

    std::string imgPath = argv[1];

    Image inputImg;
    if (!inputImg.read(imgPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    if (inputImg.format() == Image::Format::Rgb888Format) {
        std::cerr << "The image must be binary or grayscale." << std::endl;
        return 1;
    }

    std::vector<int> hist = histogram(inputImg);
    std::vector<double> histd(256);

    for (std::size_t i = 0; i < hist.size(); ++i) {
        histd[i] = hist[i];
    }

    std::string imgFullName = imgPath.substr(imgPath.find_last_of('/') + 1);
    std::string imgName = imgFullName.substr(0, imgFullName.find_last_of('.'));

    plotDirectory("res/processed/");
    plotTitle("Histogram of " + imgName);
    plotLabels("Grayscale value", "Pixel count");

    std::vector<double> points(hist.size());
    std::iota(points.begin(), points.end(), 0);

    plot(points, histd, "#888888");

    std::string outputFilename = imgName + "_histogram";

    plotWrite(outputFilename);

    return 0;
}

