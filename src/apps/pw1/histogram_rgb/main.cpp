#include <algorithm>
#include <iostream>
#include <numeric>
#include <stdexcept>
#include <string>
#include <vector>

#include "eplot/plot.h"

#include "epwdip/histogram.h"
#include "epwdip/image.h"

using namespace eplot;
using namespace epwdip;


// Histogram RGB
int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cout << "Usage : <imgPath>" << std::endl;
        return 1;
    }

    std::string imgPath = argv[1];

    Image inputImg;
    if (!inputImg.read(imgPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    if (inputImg.format() != Image::Format::Rgb888Format) {
        std::cerr << "The image must be rgb." << std::endl;
        return 1;
    }

    std::string imgFullName = imgPath.substr(imgPath.find_last_of('/') + 1);
    std::string imgName = imgFullName.substr(0, imgFullName.find_last_of('.'));

    auto hist = histogramRgb(inputImg);

    plotDirectory("res/processed/");
    plotLabels("Value", "Pixel count");
    plotTitle("Histogram of " + imgName);

    std::array<std::string, 3> curvesColors = {
        "#FF0000", "#00FF00", "#0000FF"
    };

    for (int i = 0; i < hist.size(); ++i) {
        auto cHist = hist[i];

        std::vector<double> cHistd(256);

        for (std::size_t i = 0; i < cHist.size(); ++i) {
            cHistd[i] = cHist[i];
        }

        std::vector<double> points(cHist.size());
        std::iota(points.begin(), points.end(), 0);

        plotAgain(points, cHistd, curvesColors[i]);
    }

    std::string outputFilename = imgName + "_histogram";

    plotWrite(outputFilename);

    return 0;
}
