project(histogram_rgb)

set(SOURCES
    main.cpp)

# Rules
add_executable(histogram_rgb ${SOURCES})
    
target_link_libraries(histogram_rgb epwdip eplot)

