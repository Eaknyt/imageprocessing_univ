#include <iostream>

#include "epwdip/channels.h"
#include "epwdip/histogram.h"
#include "epwdip/image.h"

using namespace epwdip;

void printCoeffs(float a, float b)
{
    std::cout << "Alpha = " << a << ", "
              << "Beta = " << b << std::endl;
}


int main(int argc, char **argv)
{
    if (argc != 3) {
        std::cout << "Usage : <imgInPath> <imgOutPath>" << std::endl;
        return 1;
    }

    std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }


    std::string imgOutPath(argv[2]);

    float a = -1;
    float b = -1;

    Image::Format format = inputImg.format();

    Image outputImg;

    if (format == Image::Format::Grayscale8Format) {
        outputImg = histExpand(inputImg, &a, &b);

        // Write results
        printCoeffs(a, b);
        outputImg.write(imgOutPath);

        system(("./histogram " + imgInPath).c_str());
        system(("./histogram " + imgOutPath).c_str());
    }
    else if (format == Image::Format::Rgb888Format) {
        Image rChannel = channel(inputImg, Channel::RedChannel);
        Image gChannel = channel(inputImg, Channel::GreenChannel);
        Image bChannel = channel(inputImg, Channel::BlueChannel);

        Image rChannelExp = histExpand(rChannel, &a, &b);
        std::cout << "Red channel : ";
        printCoeffs(a, b);

        Image gChannelExp = histExpand(gChannel, &a, &b);
        std::cout << "Green channel : ";
        printCoeffs(a, b);

        Image bChannelExp = histExpand(bChannel, &a, &b);
        std::cout << "Blue channel : ";
        printCoeffs(a, b);

        outputImg = assemble(rChannelExp, gChannelExp, bChannelExp);
        outputImg.write(imgOutPath);

        // Write results
        system(("./histogram_rgb " + imgInPath).c_str());
        system(("./histogram_rgb " + imgOutPath).c_str());
    }

    return 0;
}