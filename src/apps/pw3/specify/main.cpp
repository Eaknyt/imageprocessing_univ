#include <iostream>
#include <numeric>

#include "eplot/plot.h"

#include "epwdip/histogram.h"
#include "epwdip/image.h"

using namespace eplot;
using namespace epwdip;

int main(int argc, char **argv)
{
    if (argc != 3) {
        std::cout << "Usage : <imgInPath> <imgOutPath>" << std::endl;
        return 1;
    }

    std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    if (inputImg.format() == Image::Format::Rgb888Format) {
        std::cerr << "The image must be grayscale." << std::endl;
        return 1;
    }

    Image refImg("res/lena.pgm");

    const std::size_t N = 256;

    std::string imgOutPath(argv[2]);


    // Histogram of the output image
    system(("./histogram " + imgOutPath).c_str());

    return 0;
}