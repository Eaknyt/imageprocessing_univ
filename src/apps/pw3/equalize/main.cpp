#include <iostream>
#include <numeric>

#include "eplot/plot.h"

#include "epwdip/histogram.h"
#include "epwdip/image.h"

using namespace eplot;
using namespace epwdip;

int main(int argc, char **argv)
{
    if (argc != 3) {
        std::cout << "Usage : <imgInPath> <imgOutPath>" << std::endl;
        return 1;
    }

    std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    if (inputImg.format() == Image::Format::Rgb888Format) {
        std::cerr << "The image must be grayscale." << std::endl;
        return 1;
    }

    const std::size_t N = 256;

    std::string imgOutPath(argv[2]);

    std::vector<int> histo = histogram(inputImg);
    std::vector<double> cfd = histCfd(histo, inputImg.size());
    Image outputImg = histEqualize(inputImg, cfd);

    // Write results
    std::string imgFullName = imgInPath.substr(imgInPath.find_last_of('/') + 1);
    std::string imgOutDir = imgOutPath.substr(0, imgOutPath.find_last_of('/') + 1);
    std::string imgName = imgFullName.substr(0, imgFullName.find_last_of('.'));

    // Histogram of the input image
    system(("./histogram " + imgInPath).c_str());

    // Plot the CFD
    std::vector<double> points(N);
    std::iota(points.begin(), points.end(), 0);

    plotDirectory(imgOutDir);
    plotAgain(points, cfd, "blue");
    plotLegend("CFD");
    plotTitle("CFD of " + imgFullName);
    plotLabels("Grayscale value", "Value");
    plotWrite(imgName + "_cfd");

    // Write the output image
    outputImg.write(imgOutPath);

    // Histogram of the output image
    system(("./histogram " + imgOutPath).c_str());

    return 0;
}