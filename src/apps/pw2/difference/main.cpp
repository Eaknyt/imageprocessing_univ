#include <iostream>
#include <string>

#include "epwdip/image.h"
#include "epwdip/morpho.h"


using namespace epwdip;


int main(int argc, char *argv[])
{
    if (argc != 4) {
        std::cout << "Usage : <inputImg1> <inputImg1> <outputImgPath>" << std::endl;
        return 1;
    }

    std::string imgIn1Path(argv[1]);
    std::string imgIn2Path(argv[2]);

    Image inputImg1;
    Image inputImg2;
    if (!inputImg1.read(imgIn1Path) || !inputImg2.read(imgIn2Path)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    if (inputImg1.format() == Image::Format::Rgb888Format ||
        inputImg2.format() == Image::Format::Rgb888Format) {
        std::cerr << "The images must be binary or grayscale." << std::endl;
        return 1;
    }

    std::string imgOutPath(argv[3]);

    Image outputImg = difference(inputImg1, inputImg2);
    outputImg.write(imgOutPath);

    return 0;
}
