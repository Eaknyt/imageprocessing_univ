project(closing)

set(SOURCES
    main.cpp)

# Rules
add_executable(closing ${SOURCES})
    
target_link_libraries(closing epwdip)

