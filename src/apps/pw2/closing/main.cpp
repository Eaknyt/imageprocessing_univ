#include <iostream>
#include <string>

#include "epwdip/image.h"
#include "epwdip/morpho.h"


using namespace epwdip;


int main(int argc, char *argv[])
{
    if (argc != 3) {
        std::cout << "Usage : <imgInPath> <imgOutPath>" << std::endl;
        return 1;
    }

    std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    std::string imgOutPath(argv[2]);

    Image outputImg = closeImage(inputImg);
    outputImg.write(imgOutPath);

    return 0;
}
