#include <iostream>
#include <string>

#include "epwdip/image.h"
#include "epwdip/morpho.h"


using namespace epwdip;


int main(int argc, char *argv[])
{
    if (argc != 3) {
        std::cout << "Usage : <imgInPath> <imgOutPath>" << std::endl;
        return 1;
    }

    std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    std::string imgOutPath(argv[2]);

    Image outputImg = inputImg;

    // Dilate x6
    for (int i = 0; i < 3; ++i) {
        outputImg = dilate(outputImg);
    }

    // Erode x3
    for (int i = 0; i < 6; ++i) {
        outputImg = erode(outputImg);
    }

    // Dilate x6 again
    for (int i = 0; i < 3; ++i) {
        outputImg = dilate(outputImg);
    }

    outputImg.write(imgOutPath);

    return 0;
}
