#include <iostream>
#include <string>

#include "epwdip/image.h"
#include "epwdip/morpho.h"


using namespace epwdip;


int main(int argc, char *argv[])
{
    if (!(3 <= argc && argc <= 4)) {
        std::cout << "Usage : <imgInPath> <imgOutPath> [count]" << std::endl;
        return 1;
    }

    std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    std::string imgOutPath(argv[2]);
    int count = (argc == 4) ? std::stoi(argv[3]) : 1;

    Image outputImg = inputImg;

    for (int i = 0; i < count; ++i) {
        outputImg = dilate(outputImg);
    }

    outputImg.write(imgOutPath);

    return 0;
}
