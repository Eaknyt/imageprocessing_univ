#include <array>
#include <assert.h>
#include <cmath>
#include <iostream>

#include "epwdip/convert.h"
#include "epwdip/inpaint.h"

using namespace epwdip;


////////////////////// Main //////////////////////

int main(int argc, char **argv)
{
    if (argc != 4) {
        std::cout << "Usage : <imgInPath> <maskInPath> <imgOutPath>" << std::endl;
        return 1;
    }

    const std::string imgInPath(argv[1]);

    Image inputImg;
    if (!inputImg.read(imgInPath)) {
        std::cerr << "Invalid image path." << std::endl;
        return 1;
    }

    const std::string maskInPath(argv[2]);

    Image maskImg;
    if (!maskImg.read(maskInPath)) {
        std::cerr << "Invalid mask path." << std::endl;
        return 1;
    }

    // Compute and write output image
    const std::string imgOutPath(argv[3]);

    Image outputImg = inpaint_kernel(inputImg, maskImg, 100);

    outputImg.write(imgOutPath);

    return 0;
}
